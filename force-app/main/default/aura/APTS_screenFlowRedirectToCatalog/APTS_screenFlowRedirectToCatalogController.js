({
    init : function(component, event, helper) {      
        var urlEvent = $A.get("e.force:navigateToURL");
        var launchPage = component.get("v.launchState");
        var isCartTotalingDisabled = component.get("v.cartTotaling");
        
        if(launchPage != null) {
            urlEvent.setParams({
                "url": '/apex/Apttus_QPConfig__ProposalConfiguration?flow='+component.get("v.catalogFlowName")+'&id='+component.get("v.proposalId")+'&launchstate='+launchPage
            }); 
        } else {
            if(isCartTotalingDisabled==true) {
                urlEvent.setParams({
                    "url": '/apex/Apttus_QPConfig__ProposalConfiguration?flow='+component.get("v.catalogFlowName")+'&id='+component.get("v.proposalId")+'&isCartTotalingDisabled=true'
                });
            } else {
                urlEvent.setParams({
                    "url": '/apex/Apttus_QPConfig__ProposalConfiguration?flow='+component.get("v.catalogFlowName")+'&id='+component.get("v.proposalId")
                });
            }
        }
        urlEvent.fire();
    }
})
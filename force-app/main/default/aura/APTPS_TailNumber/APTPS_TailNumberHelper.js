({
    getRecordTypes : function(component,event,helper){
        var idrec = component.get("v.recordId");
        var action = component.get("c.getData");
        action.setParams({
            agreementid:idrec
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var returning = [];
                var dataMap = response.getReturnValue();
                //alert('dataMap '+dataMap);
                console.log('dataMap -> ' + JSON.stringify(dataMap));
                for(var key in dataMap){
                    if(dataMap[key])
                        //alert("key: " +key+" value "+dataMap[key]);
                        if(dataMap[key]=='childAgreement'){
                            component.set('v.haschildAgreement',true);
                            component.set('v.childAgreementId',key);
                            break;
                        }
                    //returning.push({value:dataMap[key],
                    //              name:key});
                }
                console.log('haschildAgreement '+component.get('v.haschildAgreement'));
                //component.set("v.availableRecordTypes",returning);
                //component.set("v.retrievedTypes",true);
            }
            else{
                let errors = [];
                    errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
                console.log("Temporary Error.");// Temporary, Will be changed to ToastError.
            }
        });
        $A.enqueueAction(action); 
    },   
    handleErrors : function(component, event, helper,error) {
        // Configure error toast
        let toastParams = {
            title: "Error",
            message: "Unknown error", // Default error message
            type: "error"
        };
        // Pass the error message if any
        if (error) {
            toastParams.message = error;
        }
         //Fire error toast
        let toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams(toastParams);
        toastEvent.fire();
    },
    saveRec : function() {
    var results = $A.get("e.force:showToast");
    results.setParams({
    "Title": "Saved",
    "message": "Tail Updated"
	});
	results.fire();		
	}
})
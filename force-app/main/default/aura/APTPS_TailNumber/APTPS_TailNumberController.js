({
	init : function(component, event, helper) {
		var idrec = component.get("v.recordId");
		console.log('AgreementId-->'+idrec);
		var items = [];
        helper.getRecordTypes(component,event,helper); 

		var action = component.get("c.getTails");
        action.setParams({
			agreementid:idrec
		});
		action.setCallback(this,function (response) {
			var state = response.getState();
            //alert(state);
			var tails = [], result = [];
			if(state === "SUCCESS"){
                result = response.getReturnValue();
				tails = JSON.parse(result)["TailList"];
				console.log('Tails-->');
                console.log(tails);
			
			var index = "";
			for (index in tails){
				var tail = tails[index];
				var item = { "label" : tail.TailNumber,"value" : tail.TailNumber};
				items.push(item);
			}
			component.set("v.tailList",items);
            } else{
				let errors = [];
                    errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
				console.log('Error-->Response State:'+state);
			}
			console.log('Out of INIT Function');
		});
		var action2 = component.get("c.getAgreementExtension");
		action2.setParams({
			agreementid:idrec
		});
		action2.setCallback(this,function (response) {
			var state = response.getState();
			if(state === "SUCCESS"){
				var recext = response.getReturnValue();
				component.set("v.oAgreementExtension",recext);
				component.set("v.tailInfoReady",'TRUE');
				//console.log('AgreementExtensionRec-->'+recext);
			} else{
                let errors = [];
                    errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
				console.log('Error-->Response State:'+state);
			}		
			console.log('Out of Action2 INIT Function');
		});
		$A.enqueueAction(action);
		$A.enqueueAction(action2);	
	},
    updateInterim: function(component, event, helper){
        var interim = component.get('v.tailNumberinterim');
        var idrec = component.get("v.childAgreementId");
		console.log('AgreementId-->'+idrec);
		//component.set("v.selectedTail",selectedTail);
		var action = component.get("c.populateTailInformation");
		action.setParams({
			tailNumber:interim,
			agreementid:idrec
		});
		action.setCallback(this,function (response) {
			var state = response.getState();
			var tail_details = [];
			if(state === "SUCCESS"){
				var recext = response.getReturnValue();
				console.log('AgreementExtension-->'+recext);
				component.set("v.oChildAgreementExtension",recext);
                component.set("v.interimtailInfoReady",'TRUE');
                
				
			}else{
                let errors = [];
                    errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
				console.log('Error-->Response State:'+state);
			}			
		});
		$A.enqueueAction(action);

    },
	updateTail : function(component, event, helper) {
        
        var selectedTail;
        
		selectedTail = event.getParam("value");
        //alert('Selected Tail-->'+selectedTail);
        
		var idrec = component.get("v.recordId");
		console.log('AgreementId-->'+idrec);
		component.set("v.selectedTail",selectedTail);
		var action = component.get("c.populateTailInformation");
		action.setParams({
			tailNumber:selectedTail,
			agreementid:idrec
		});
		action.setCallback(this,function (response) {
			var state = response.getState();
			var tail_details = [];
			if(state === "SUCCESS"){
				var recext = response.getReturnValue();
				console.log('AgreementExtension-->'+recext);
				component.set("v.oAgreementExtension",recext);
                
                component.set("v.tailInfoReady",'TRUE'); 
				
			}else{
                let errors = [];
                    errors = response.getError();
                let error;
                // Retrieve the error message sent by the server
                if (errors) {
                    error = errors[0].message;
                }
                // Display the message      
                console.error( errors[0].message);
                helper.handleErrors(component, event, helper,error);
				console.log('Error-->Response State:'+state);
			}			
		});
		$A.enqueueAction(action);

        //helper.saveRec();
	}
    
})
/************************************************************************************************************************
@Name: APTPS_EmailNotificationTest
@Author: Conga PS Dev Team
@CreateDate: 01 June 2021
@Description: Test class coverage for Email Notification implementation
************************************************************************************************************************/
@isTest
public class APTPS_EmailNotificationTest {
    @testsetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTS_Transaction_Type__c = 'Demo Flight';
        demoNJUSQuote.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        demoNJUSQuote.Apttus_QPApprov__Approval_Status__c = 'Approval Required';
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTS_Transaction_Type__c = 'Demo Flight';
        demoNJUSQuote.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        demoNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approval Required';
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.APTS_Transaction_Type__c = 'Corporate Trial';
        corpNJEQuote.APTPS_Program_Type__c = APTS_ConstantUtil.CORP_TRIAL;
        corpNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approval Required';
        proposalList.add(corpNJEQuote);
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c demoUS = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoUS.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoUS.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoUS.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoUS);
        
        Apttus_Proposal__Proposal_Line_Item__c demoNJE = APTS_CPQTestUtility.getPropLI(NJE_DEMO.Id, 'New', demoNJEQuote.Id, '', null);
        demoNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoNJE.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoNJE);
        
        Apttus_Proposal__Proposal_Line_Item__c corporateNJE = APTS_CPQTestUtility.getPropLI(NJE_CORP.Id, 'New', corpNJEQuote.Id, '', null);
        corporateNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        corporateNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        corporateNJE.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(corporateNJE);
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJUSPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJUSPav.Apttus_QPConfig__LineItemId__c = demoUS.Id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJUSPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJEPav.Apttus_QPConfig__LineItemId__c = demoNJE.Id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJEPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c corporateNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        corporateNJEPav.Apttus_QPConfig__LineItemId__c = corporateNJE.Id;
        corporateNJEPav.APTPS_Corporate_Trial_Aircraft__c = 'Citation Latitude';
        corporateNJEPav.APTPS_Term_Months__c = String.valueOf(3);
        corporateNJEPav.APTPS_Term_End_Date__c = Date.today();
        corporateNJEPav.APTPS_Maximum_Number_of_Trials__c = 2.0;
        corporateNJEPav.APTPS_Override_Deposit__c = 100.00;
        pavList.add(corporateNJEPav);
        insert pavList;

        Test.stopTest();
    }
    
    @isTest
    public static void testEmailNotifation() {
        Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id,APTPS_Program_Type__c,CurrencyIsoCode,Apttus_Proposal__Account__c, Apttus_QPConfig__ConfigurationFinalizedDate__c,Apttus_QPApprov__Approval_Status__c,Apttus_Proposal__Primary_Contact__c  FROM Apttus_Proposal__Proposal__c 
                                                      WHERE Apttus_Proposal__Proposal_Name__c = 'DEMO NJUS Quote' LIMIT 1];
        demoNJUSQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        demoNJUSQuote.Apttus_QPApprov__Approval_Status__c = 'Approved';
        quotesToUpdate.add(demoNJUSQuote);
        
        contact con = APTS_CPQTestUtility.createContact('test', 'contact', demoNJUSQuote.Apttus_Proposal__Account__c);
        insert  con;
        Apttus__APTS_Admin__c adminEntry = new Apttus__APTS_Admin__c(Name='APTS_DefaultEmailContactName',Apttus__Value__c='test contact');
        insert adminEntry ;
        /*Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE Apttus_Proposal__Proposal_Name__c = 'DEMO NJE Quote' LIMIT 1];
        demoNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        demoNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approved';
        quotesToUpdate.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corporateNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE Apttus_Proposal__Proposal_Name__c = 'Corporate NJE Quote' LIMIT 1];
        corporateNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        corporateNJEQuote.Apttus_QPApprov__Approval_Status__c = 'Approved';
        quotesToUpdate.add(corporateNJEQuote);*/
        
        
        try{
            /*APTPS_EmailNotification testObj = new APTPS_EmailNotification();
            testObj.call(APTS_ConstantUtil.NJUS_DEMO, new Map<String, Object>{'Id' => quotesToUpdate[0].Id,'Business_Object'=>APTS_ConstantUtil.QUOTE_OBJ,'Product_Code'=>APTS_ConstantUtil.NJUS_DEMO,'Status'=>'Approved','ContactId'=>quotesToUpdate[0].Apttus_Proposal__Primary_Contact__c});*/
            Map<Id, Apttus_Proposal__Proposal__c> enQuotesInScope = new Map<Id, Apttus_Proposal__Proposal__c>();
            enQuotesInScope.put(demoNJUSQuote.id,demoNJUSQuote);
            APTS_QuoteProposalBaseTriggerHelper.processEmailNotification(enQuotesInScope);
            //update quotesToUpdate;
        }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
            system.debug('Test coverage for exception');
        }
        
        Test.stopTest();
    }
}
/************************************************************************************************************************
@Name: APTPS_QuoteCollaborationTest
@Author: Conga PS Dev Team
@CreateDate: 26 July 2021
@Description: Test class coverage for Share & Lease Quote Collaboration process.
************************************************************************************************************************/
@isTest
public class APTPS_QuoteCollaborationTest {
    @TestSetup
    static void makeData(){
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial,Share';
        insert snlProducts;
        
        Apttus_Config2__ConfigCustomClasses__c configCustomClasses = new Apttus_Config2__ConfigCustomClasses__c();
        configCustomClasses.Name = 'Custom Classes';
        configCustomClasses.Apttus_Config2__ValidationCallbackClass__c = 'APTS_ValidationCallback';
        insert configCustomClasses;
        
        Apttus_Config2__ConfigLineItemCustomFields__c liFields = new Apttus_Config2__ConfigLineItemCustomFields__c();
        liFields.Name = 'SNL Pricing Fields';
        liFields.Apttus_Config2__CustomFieldNames2__c = 'Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.Program__c,Apttus_Config2__ConfigurationId__r.APTPS_Program_Type__c';
        liFields.Apttus_Config2__CustomFieldNames__c = 'Apttus_Config2__ConfigurationId__r.Apttus_Config2__BusinessObjectType__c,Apttus_Config2__ConfigurationId__r.APTPS_Is_Quote_Collaboration_Completed__c';
        liFields.Apttus_Config2__CustomFieldNames3__c = 'Apttus_Config2__OptionId__r.ProductCode,Apttus_Config2__AttributeValueId__r.APTPS_Service_Account__c';
        insert liFields;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1];
        USer newUser = new User(lastname = 'Test',Alias = 'Test',TimeZoneSidKey = 'GMT',
                                LocaleSidKey = 'eu_ES',EmailEncodingKey = 'ISO-8859-1',
                                ProfileId = p.id,LanguageLocaleKey = 'en_US',
                                userName='TestUser@netjets.com',email='Test@netjets.com');
        insert newUser;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJA_SHARE);
        
        Product2 PHENOM_NJUS = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Apttus', 'Option', true, true, true, true);
        PHENOM_NJUS.Family = 'Aircraft';
        prodList.add(PHENOM_NJUS);
        insert prodList;
        
        List<Apttus_Config2__PriceListItem__c> pList = new List<Apttus_Config2__PriceListItem__c>();
        Apttus_Config2__PriceListItem__c shareAcPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, PHENOM_NJUS.Id, 1000.0, 'Purchase Price', 'One Time', 'Per Unit', 'Each', true);
        pList.add(shareAcPLI);
        insert pList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c shareNJUSQuote = APTS_CPQTestUtility.createProposal('Share NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJUSQuote.APTPS_Program_Type__c = 'Share';
        shareNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(shareNJUSQuote);
        insert proposalList;
        
        Id shareConfig = APTS_CPQTestUtility.createConfiguration(shareNJUSQuote.Id);
        
        List<Apttus_Config2__LineItem__c> liList = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__LineItem__c liShareAC = APTS_CPQTestUtility.getLineItem(shareConfig, shareNJUSQuote, shareAcPLI, PHENOM_NJUS, PHENOM_NJUS.Id, PHENOM_NJUS.Id, 'Option', 1, 1, 1, 0, null, 1);
        liList.add(liShareAC);
        insert liList;
        
        //Create Attribute Value
        List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
        Apttus_Config2__ProductAttributeValue__c shareNJUSAcPAV = new Apttus_Config2__ProductAttributeValue__c();
        shareNJUSAcPAV.Apttus_Config2__LineItemId__c = liShareAC.Id;
        shareNJUSAcPAV.APTPS_Vintage__c = '2015';
        shareNJUSAcPAV.APTPS_Hours__c = '50';
        shareNJUSAcPAV.APTPS_Percentage__c = 50;
        shareNJUSAcPAV.APTPS_Contract_Length__c = 60.0;
        shareNJUSAcPAV.APTPS_Delayed_Start_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Minimum_Commitment_Period__c = '36';
        shareNJUSAcPAV.APTPS_End_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Delayed_Start_Months__c = '2';
        pavList.add(shareNJUSAcPAV);
        insert pavList;
        
        Apttus_Config2__CollaborationRequest__c collabReq = new Apttus_Config2__CollaborationRequest__c();
        collabReq.Apttus_Config2__ChildConfigurationId__c = shareConfig;
        collabReq.Apttus_Config2__CollaborationType__c = 'Peer-To-Peer';
        collabReq.Apttus_Config2__ParentConfigurationId__c = shareConfig;
        collabReq.Apttus_Config2__ParentBusinessObjectType__c = 'Proposal';
        collabReq.Apttus_Config2__ParentBusinessObjectId__c = shareNJUSQuote.Id;
        collabReq.Apttus_Config2__PriceListId__c = testPriceList.Id;
        collabReq.Apttus_Config2__Status__c = 'New';
        collabReq.CurrencyIsoCode = 'USD';
        insert collabReq;
        Test.stopTest();
    }
    
    @isTest
    public static void testCollaboration(){
        Test.startTest();
        Apttus_Proposal__Proposal__c shareNJA = [SELECT ID FROM Apttus_Proposal__Proposal__c 
                                                 WHERE APTPS_Program_Type__c = 'Share' 
                                                 AND CurrencyIsoCode = 'USD' LIMIT 1];
        
        Apttus_Config2__CollaborationRequest__c collabReq = [SELECT Id, Apttus_Config2__Status__c, OwnerId 
                                                             FROM Apttus_Config2__CollaborationRequest__c 
                                                             WHERE Apttus_Config2__ParentBusinessObjectId__c =: shareNJA.Id LIMIT 1];
        
        User testUser = [SELECT Id FROM User Where UserName = 'TestUser@netjets.com' LIMIT 1];
        
        Apttus_Config2__ProductConfiguration__c config = [SELECT Id, APTPS_Is_Quote_Collaboration_Completed__c FROM Apttus_Config2__ProductConfiguration__c 
                                                          WHERE Apttus_QPConfig__Proposald__c =: shareNJA.Id LIMIT 1];
        
        collabReq.Apttus_Config2__Status__c = APTS_ConstantUtil.STATUS_SUBMITTED;
        update collabReq;
        
        collabReq.OwnerId = testUser.Id;
        update collabReq;
        
        collabReq.Apttus_Config2__Status__c = APTS_ConstantUtil.STATUS_COMPLETED;
        update collabReq;
        
        collabReq.Apttus_Config2__Status__c = APTS_ConstantUtil.STATUS_ACCEPTED;
        update collabReq;
        
        //START:Email Controller test coverage
        APTPS_QuoteCollaborationEmailController controller = new APTPS_QuoteCollaborationEmailController();
        controller.qId = shareNJA.Id;
        controller.collabId = collabReq.Id;
        controller.mode = 'Submitted';
        controller.parentConfigId = config.Id;
        controller.childConfigId = config.Id;
        controller.getDetails();
        //END: Email Controller test converage
		
        //START: Quote Collaboration validation test coverage
       	config.APTPS_Is_Quote_Collaboration_Completed__c = false;
        update config;
        Apttus_Config2.RuntimeContext.getParameters().put('pageAction', APTS_ConstantUtil.PAGE_ACTION);
        Apttus_Config2.CustomClass.ValidationResult result = Apttus_Config2.CPQWebService.validateCart(config.Id);
        System.AssertEquals(false, result.isSuccess);
        //END: Quote Collaboration validation test coverage
        
        delete collabReq;
        Test.stopTest();
    }
}
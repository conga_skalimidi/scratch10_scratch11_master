/************************************************************************************************************************
@Name: APTPS_ApprovalsTest
@Author: Conga PS Dev Team
@CreateDate: 28 June 2021
@Description: Test class coverage for Approvals implementation
************************************************************************************************************************/
@isTest
public class APTPS_ApprovalsTest {
    @testsetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;

        Service_Account__c serviceAcc = new Service_Account__c();
        serviceAcc.Account__c = accToInsert.Id;
        serviceAcc.NetJets_Company__c = 'NJA';
        serviceAcc.Status__c = 'Active';
        insert serviceAcc;

        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
        Product2 NJUS_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJUS_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJUS_DEMO);
        
        Product2 NJE_DEMO = APTS_CPQTestUtility.createProduct('Demo', 'NJE_DEMO', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJE_DEMO);
        
        Product2 NJUS_FLIGHT_INFO = APTS_CPQTestUtility.createProduct('Flight Info', 'NJUS_FLIGHT_INFO', 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJUS_FLIGHT_INFO);
        
        Product2 NJUS_RET_FLIGHT_INFO = APTS_CPQTestUtility.createProduct('Flight Return Info', 'NJUS_RET_FLIGHT_INFO', 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJUS_RET_FLIGHT_INFO);
        
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);

        //START: NJA Share product test setup
        Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJA_SHARE);

        Product2 NJA_INCENTIVE = APTS_CPQTestUtility.createProduct('Purchase Price Incentives', APTS_ConstantUtil.PRICE_INCENTIVE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_INCENTIVE);

        Product2 NJA_ACC_FEES = APTS_CPQTestUtility.createProduct('ACC Fees', APTS_ConstantUtil.ACC_FEE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_ACC_FEES);

        Product2 NJA_INTERIM_LEASE = APTS_CPQTestUtility.createProduct('Interim Lease', APTS_ConstantUtil.INTERIM_LEASE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_INTERIM_LEASE);

        Product2 NJA_SPLIT_BILL = APTS_CPQTestUtility.createProduct('Interim Lease', APTS_ConstantUtil.SPLIT_BILL_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_SPLIT_BILL);

        Product2 NJA_PREPAY_FEE = APTS_CPQTestUtility.createProduct('Interim Lease', APTS_ConstantUtil.PREPAY_FEE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_PREPAY_FEE);

        Product2 PHENOM_NJUS = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Apttus', 'Option', true, true, true, true);
        PHENOM_NJUS.Family = 'Aircraft';
        prodList.add(PHENOM_NJUS);
        //END: NJA Share product test setup
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('DEMO NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = 'Demo';
        demoNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = APTS_CPQTestUtility.createProposal('DEMO NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJEQuote.APTPS_Program_Type__c = 'Demo';
        //demoNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.APTPS_Program_Type__c = 'Corporate Trial';
        //corpNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(corpNJEQuote);

        Apttus_Proposal__Proposal__c shareNJUSQuote = APTS_CPQTestUtility.createProposal('Share NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJUSQuote.APTPS_Program_Type__c = 'Share';
        shareNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(shareNJUSQuote);
        
        Apttus_Proposal__Proposal__c shareNJEQuote = APTS_CPQTestUtility.createProposal('Share NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJEQuote.APTPS_Program_Type__c = 'Share';
        //shareNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(shareNJEQuote);
        insert proposalList;
        
        //Create Quote/Proposal Line Items
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c demoUS = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoUS.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoUS.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoUS.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoUS);
        
        Apttus_Proposal__Proposal_Line_Item__c demoNJE = APTS_CPQTestUtility.getPropLI(NJE_DEMO.Id, 'New', demoNJEQuote.Id, '', null);
        demoNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        demoNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoNJE.Apttus_QPConfig__NetPrice__c = 123;
        pliList.add(demoNJE);
        
        Apttus_Proposal__Proposal_Line_Item__c demoFlightInfo = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoFlightInfo.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        demoFlightInfo.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoFlightInfo.Apttus_QPConfig__OptionId__c = NJUS_FLIGHT_INFO.Id;
        demoFlightInfo.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(demoFlightInfo);
        
         Apttus_Proposal__Proposal_Line_Item__c demoFlightRetInfo = APTS_CPQTestUtility.getPropLI(NJUS_DEMO.Id, 'New', demoNJUSQuote.Id, '', null);
        demoFlightInfo.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        demoFlightInfo.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        demoFlightInfo.Apttus_QPConfig__OptionId__c = NJUS_RET_FLIGHT_INFO.Id;
        demoFlightInfo.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(demoFlightRetInfo);
        
        Apttus_Proposal__Proposal_Line_Item__c corporateNJE = APTS_CPQTestUtility.getPropLI(NJE_CORP.Id, 'New', corpNJEQuote.Id, '', null);
        corporateNJE.Apttus_QPConfig__LineType__c = 'Product/Service';
        corporateNJE.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        corporateNJE.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(corporateNJE);
        
        //START: Share PLI test setup
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSFET = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.LINE_TYPE;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.FEDERAL_EXCISE_TAX;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSFET);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSMMF = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.MMF_CHARGE_TYPE;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSMMF);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSOF = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.OF_CHARGE_TYPE;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSOF);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSSR1 = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.SR1_CHARGE_TYPE;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSSR1);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSSR2 = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.SR2_CHARGE_TYPE;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSSR2);
        
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSOHF = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.OHF_CHARGE_TYPE;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSOHF);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSIncentive = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSIncentive.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSIncentive.Apttus_QPConfig__OptionId__c = NJA_INCENTIVE.Id;
        shareNJUSIncentive.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSIncentive);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSAcc = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSAcc.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSAcc.Apttus_QPConfig__OptionId__c = NJA_ACC_FEES.Id;
        shareNJUSAcc.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSAcc);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSInterim = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSInterim.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSInterim.Apttus_QPConfig__OptionId__c = NJA_INTERIM_LEASE.Id;
        shareNJUSInterim.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSInterim);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSSplitBill = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSSplitBill.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSSplitBill.Apttus_QPConfig__OptionId__c = NJA_SPLIT_BILL.Id;
        shareNJUSSplitBill.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSSplitBill);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSPrePay = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSPrePay.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSPrePay.Apttus_QPConfig__OptionId__c = NJA_PREPAY_FEE.Id;
        shareNJUSPrePay.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSPrePay);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSAc = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSAc.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.PURCHASE_PRICE;
        shareNJUSAc.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSAc.Apttus_QPConfig__OptionId__c = PHENOM_NJUS.Id;
        shareNJUSAc.Apttus_QPConfig__NetPrice__c = 1000;
        pliList.add(shareNJUSAc);
        //END: Share PLI test setup
        insert pliList;
        
        //Create Proposal Attribute Value
        List<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new List<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJUSPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJUSPav.Apttus_QPConfig__LineItemId__c = demoUS.Id;
        demoNJUSPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJUSPav.APTPS_Demo_Aircraft__c = 'Citation XLS';
        demoNJUSPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJUSPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c demoNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        demoNJEPav.Apttus_QPConfig__LineItemId__c = demoNJE.Id;
        demoNJEPav.APTPS_Type_of_Demo__c = 'Card';
        demoNJEPav.APTPS_Demo_Aircraft__c = 'Phenom 300';
        demoNJEPav.APTPS_Rate_Type__c = 'Non-Premium';
        pavList.add(demoNJEPav);
        
        Apttus_QPConfig__ProposalProductAttributeValue__c corporateNJEPav = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        corporateNJEPav.Apttus_QPConfig__LineItemId__c = corporateNJE.Id;
        corporateNJEPav.APTPS_Corporate_Trial_Aircraft__c = 'Citation Latitude';
        corporateNJEPav.APTPS_Term_Months__c = String.valueOf(3);
        corporateNJEPav.APTPS_Term_End_Date__c = Date.today();
        corporateNJEPav.APTPS_Maximum_Number_of_Trials__c = 2.0;
        corporateNJEPav.APTPS_Override_Deposit__c = 100.00;
        pavList.add(corporateNJEPav);

        //START: Share PAV test setup
        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSIncentivePAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSIncentivePAV.Apttus_QPConfig__LineItemId__c = shareNJUSIncentive.Id;
        shareNJUSIncentivePAV.APTPS_Incentive_Amount__c = 1000;
        pavList.add(shareNJUSIncentivePAV);

        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSAccPAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSAccPAV.Apttus_QPConfig__LineItemId__c = shareNJUSAcc.Id;
        shareNJUSAccPAV.APTPS_Fee_Amount__c = 1000;
        pavList.add(shareNJUSAccPAV);

        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSSplitBillPAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSSplitBillPAV.Apttus_QPConfig__LineItemId__c = shareNJUSSplitBill.Id;
        shareNJUSSplitBillPAV.APTPS_Service_Account__c = serviceAcc.Id;
        shareNJUSSplitBillPAV.APTPS_Allocated_Hours__c = 25;
        shareNJUSSplitBillPAV.APTPS_Percent__c = 50;
        pavList.add(shareNJUSSplitBillPAV);

        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSAcPAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSAcPAV.Apttus_QPConfig__LineItemId__c = shareNJUSAc.Id;
        shareNJUSAcPAV.APTPS_Vintage__c = '2015';
        shareNJUSAcPAV.APTPS_Hours__c = '50';
        shareNJUSAcPAV.APTPS_Percentage__c = 50;
        shareNJUSAcPAV.APTPS_Contract_Length__c = 60.0;
        shareNJUSAcPAV.APTPS_Delayed_Start_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Minimum_Commitment_Period__c = '36';
        shareNJUSAcPAV.APTPS_End_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Delayed_Start_Months__c = '2';
        pavList.add(shareNJUSAcPAV);
        //END: Share PAV test setup
        insert pavList;

        Test.stopTest();
    }
    
    @isTest
    public static void testApprovals() {
        Test.startTest();
        List<Apttus_Proposal__Proposal__c> quotesToUpdate = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                      WHERE APTPS_Program_Type__c = 'Demo'  LIMIT 1];
        demoNJUSQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJUSQuote);
        
        Apttus_Proposal__Proposal__c demoNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                     WHERE APTPS_Program_Type__c = 'Demo'  LIMIT 1];
        demoNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(demoNJEQuote);
        
        Apttus_Proposal__Proposal__c corporateNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Corporate Trial' LIMIT 1];
        corporateNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(corporateNJEQuote);

        //START: Deal Summary test coverage for Share
        Apttus_Proposal__Proposal__c shareNJAQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Share' LIMIT 1];
        shareNJAQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        quotesToUpdate.add(shareNJAQuote);
        
        Apttus_Proposal__Proposal__c shareNJEQuote = [SELECT Id, Apttus_QPConfig__ConfigurationFinalizedDate__c FROM Apttus_Proposal__Proposal__c 
                                                          WHERE APTPS_Program_Type__c = 'Share' LIMIT 1];
        shareNJEQuote.Apttus_QPConfig__ConfigurationFinalizedDate__c = Date.today();
        //quotesToUpdate.add(shareNJEQuote);
        
        
        system.debug('Quote List --> '+quotesToUpdate);
        update quotesToUpdate;
        
        
        
        Test.stopTest();
    }
}
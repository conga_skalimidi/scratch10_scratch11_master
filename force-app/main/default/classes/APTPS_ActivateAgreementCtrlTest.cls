/************************************************************************************************************************
@Name: APTPS_ActivateAgreementCtrlTest
@Author: Conga PS Dev Team
@CreateDate: 07 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_ActivateAgreementCtrlTest {
    @testSetup
    public static void makeData(){
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
        List<Product2> prodList = new List<Product2>();
       
        Product2 NJE_CORP = APTS_CPQTestUtility.createProduct('Corporate Trial', 'NJE_Corporate_Trial', 'Apttus', 'Standalone', true, true, true, true);
        prodList.add(NJE_CORP);
        insert prodList;
        
        List<Apttus_Config2__PriceListItem__c> pList = new List<Apttus_Config2__PriceListItem__c>();
        Apttus_Config2__PriceListItem__c demoNJEPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, NJE_CORP.Id, 0.0, 'Demo Price', 'One Time', 'Per Unit', 'Each', true);
        pList.add(demoNJEPLI);
        insert pList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
               
        Apttus_Proposal__Proposal__c corpNJEQuote = APTS_CPQTestUtility.createProposal('CLM Automation Corporate NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        corpNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        corpNJEQuote.CurrencyIsoCode = APTS_ConstantUtil.CUR_EUR;
        proposalList.add(corpNJEQuote);
        insert proposalList;
        
       /* List<Attachment> docList = new List<Attachment>();
        for(Apttus_Proposal__Proposal__c qObj : proposalList) {
            Attachment dummyAttachment = new Attachment();
            dummyAttachment.Name = 'Test Title';
            dummyAttachment.Body = Blob.valueOf('Test Body');
            dummyAttachment.ContentType = 'text/plain';
            dummyAttachment.ParentId = qObj.Id;
            docList.add(dummyAttachment);
        }
        insert docList;*/
        
        
        
        //Corporate NJE Agreement lifecycle test data
        Apttus__APTS_Agreement__c ctAg = new Apttus__APTS_Agreement__c();
        ctAg.Name = 'CLM Automation Corporate NJE Agreement';
        ctAg.Agreement_Status__c = 'Draft';
        ctAg.Apttus__Account__c = accToInsert.Id;
        ctAg.Apttus_QPComply__RelatedProposalId__c = corpNJEQuote.Id;
        ctAg.APTPS_Program_Type__c = APTS_ConstantUtil.CORP_TRIAL;
        ctAg.Apttus__Status__c = 'Request';
        ctAg.Apttus__Status_Category__c = 'Request';
        ctAg.CurrencyIsoCode = 'EUR';
        ctAg.APTPS_Term_Months__c = String.valueOf(6);
        ctAg.APTPS_Term_End_Date__c = Date.today();
        insert ctAg;

        Test.stopTest();
    }
    
    @isTest
    public static void testActivation(){
        Test.startTest();
        
        //Corporate NJE Agreement lifecycle testing
        Apttus__APTS_Agreement__c ctAg = [SELECT Id, Apttus__Status__c, Apttus__Status_Category__c FROM Apttus__APTS_Agreement__c 
                                         WHERE APTPS_Program_Type__c =:APTS_ConstantUtil.CORP_TRIAL LIMIT 1];
        ctAg.Apttus__Status__c = 'Ready for Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        Payment_Entry__c payObj = new Payment_Entry__c();
        payObj.Agreement__c = ctAg.Id;
        payObj.CurrencyIsoCode = 'EUR';
        payObj.Payment_Amount__c = 1000;
        payObj.Payment_Date__c = Date.today();
        payObj.Payment_Method__c = 'Wire';
        insert payObj;
        
        ctAg.Apttus__Status__c = 'Other Party Signatures';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
        
        ctAg.Apttus__Status__c = 'Fully Signed';
        ctAg.Apttus__Status_Category__c = 'In Signatures';
        update ctAg;
       APTPS_ActivateAgreementCtrl.activateAgreement(ctAg.Id);
        Test.stopTest();
    }
}
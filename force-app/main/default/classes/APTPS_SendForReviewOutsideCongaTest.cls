/***********************************************************************************************************************
@Name: APTPS_SendForReviewOutsideCongaTest
@Author: Avinash Bamane
@CreateDate: 09 August 2021
@Description: Test class for APTPS_SendForReviewOutsideCongaCtrl
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/
@isTest
public class APTPS_SendForReviewOutsideCongaTest {
    @TestSetup
    static void makeData(){
        Test.startTest();
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        Apttus_Proposal__Proposal__c testProposal = APTS_CPQTestUtility.createProposal('test Apttus Proposal', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        testProposal.APTS_New_Sale_or_Modification__c = 'New Sale';
        testProposal.APTPS_Program_Type__c = 'Share';
        insert testProposal;
        
        Apttus__APTS_Agreement__c ag = APTS_CPQTestUtility.createAgreement('Test Agreement', APTS_ConstantUtil.AGREEMENT_CONTRACT_GENERATED, accToInsert.Id, testProposal.Id);
        ag.APTS_New_Sale_or_Modification__c = 'New Sale';
        ag.APTPS_Program_Type__c = 'Share';
        insert ag;
        Test.stopTest();
    }
    
    @isTest
    static void testSendForReviewOutsideConga(){
        Test.startTest();
        Apttus__APTS_Agreement__c ag = [SELECT Id FROM Apttus__APTS_Agreement__c LIMIT 1];
        APTPS_SendForReviewOutsideCongaCtrl.updateReviewOutsideCongaRequest(ag.Id);
        
        Apttus__APTS_Agreement__c resultAg = [SELECT Id, APTPS_Sent_For_Review_Outside_Conga__c FROM Apttus__APTS_Agreement__c WHERE Id =: ag.Id];
        system.assert(resultAg.APTPS_Sent_For_Review_Outside_Conga__c);
        
        try{
            Apttus_Proposal__Proposal__c quote = [SELECT Id FROM Apttus_Proposal__Proposal__c LIMIT 1];
            APTPS_SendForReviewOutsideCongaCtrl.updateReviewOutsideCongaRequest(quote.Id);
        } catch(Exception e) {
            system.debug('Code coverage for error block');
        }
        Test.stopTest();
    }
}
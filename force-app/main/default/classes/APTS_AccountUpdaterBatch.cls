/*
* Class Name: APTS_AccountUpdaterBatch
* Description: 
* 1. Batch APEX class to update field on Account Object.
* Business Logic - Currently, this Batch APEX is geeting executed from APTS_AccountUpdateScheduler Scheduler. Batch APEX will set APTS_Has_Active_Agreement__c to FALSE for those accounts
* where there is no active Agreement is available.
* 2. Set 'Has Active NJUS Agreement?' or 'Has Active NJE Agreement?' field on Account. Discard the approach to stamp 
* 'Has Active Agreement?' field.
* Created By: DEV Team, Apttus

* Modification Log
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Developer               Date           US/Defect        Description
* ------------------------------------------------------------------------------------------------------------------------------------------------------------
* Avinash Bamane		 13/12/19		  GCM-6804 			 Added
* Avinash Bamane		 29/07/20		  GCM-8415			 Updated
* Avinash Bamane		 07/10/20		  GCM-9725			 Updated
* Avinash Bamane		 14/10/20		  GCM-9745			 Updated
*/

public with sharing class APTS_AccountUpdaterBatch implements Database.Batchable<sObject>, Database.Stateful{
    private integer recordCount = 0;
    private integer processedRecordCount = 0;
    private string STATUS_CATEGORY = 'In Effect';
    private string STATUS = 'Activated';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
       	//START: GCM-9725
       	//Inner SOQL has limit of 200 records. It's a Salesforce limitation. For accounts where there are more than 200 agreements, it's giving error.
       	//To overcome this limitation, batch logic has been updated.
        //return Database.getQueryLocator('SELECT Id, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c, (SELECT Id, Program__c FROM R00N50000001Xl0UEAS WHERE Apttus__Status_Category__c = :STATUS_CATEGORY AND Apttus__Status__c = :STATUS) FROM Account');
        return Database.getQueryLocator('SELECT Id, APTS_Has_Active_NJUS_Agreement__c, APTS_Has_Active_NJE_Agreement__c FROM Account WHERE APTS_Has_Active_NJUS_Agreement__c = TRUE OR APTS_Has_Active_NJE_Agreement__c = TRUE');
        //END: GCM-9725
    }
    
    public void execute(Database.BatchableContext BC, List<Account> scope) {
        recordCount += scope.size();
        system.debug('Account records to be processed --> '+scope.size());
        List<Account> accList = new List<Account>();
        //START: GCM-9725
        Map<Id, Account> accMap = new Map<Id, Account>();
        for(Account acc : scope) {
            accMap.put(acc.Id, acc);
        }
        
        Map<Id, List<Apttus__APTS_Agreement__c>> accAgreementMap = new Map <Id, List<Apttus__APTS_Agreement__c>>();
        if(accMap != null && !accMap.isEmpty()) {
            for(Apttus__APTS_Agreement__c agObj : [SELECT Id, Apttus__Account__c, Program__c FROM Apttus__APTS_Agreement__c 
                                                   WHERE Apttus__Account__c IN :accMap.keySet() 
                                                   AND Apttus__Status_Category__c = :STATUS_CATEGORY 
                                                   AND Apttus__Status__c = :STATUS]) {
                                                       if(accAgreementMap != null && !accAgreementMap.isEmpty() && accAgreementMap.containsKey(agObj.Apttus__Account__c)) 
                                                           accAgreementMap.get(agObj.Apttus__Account__c).add(agObj);
                                                       else 
                                                           accAgreementMap.put(agObj.Apttus__Account__c, new List<Apttus__APTS_Agreement__c>{agObj});
    
            }
        }
		//END: GCM-9725
		//Next logic is updated on the basis of GCM-9725 revised logic
        
        system.debug('Account-Agreement Map size --> '+ accAgreementMap.size());
        if(accAgreementMap != null && !accAgreementMap.isEmpty()) {
            for(Id accId : accMap.keySet()) {
                Account accToUpdate = new Account(Id = accId);
                //START: GCM-9745
                Account accObj = accMap.containsKey(accId) ? accMap.get(accId) : null;
                boolean hasActiveNJUSFlag = accObj != NULL ? accObj.APTS_Has_Active_NJUS_Agreement__c : false;
                boolean hasActiveNJEFlag = accObj != NULL ? accObj.APTS_Has_Active_NJE_Agreement__c : false;
                //END: GCM-9745
                List<Apttus__APTS_Agreement__c> agList = accAgreementMap.containsKey(accId) && accAgreementMap.get(accId) != null ? accAgreementMap.get(accId) : null;
                
                //GCM-9745: Added condition to check current value of both flags.
                if(agList == null && (hasActiveNJUSFlag || hasActiveNJEFlag)) {
                    accToUpdate.APTS_Has_Active_NJUS_Agreement__c = false;
                    accToUpdate.APTS_Has_Active_NJE_Agreement__c = false;
                    accList.add(accToUpdate);
                } else {
                    boolean NJUS_AG_FOUND = FALSE;
                    boolean NJE_AG_FOUND = FALSE;
                    for(Apttus__APTS_Agreement__c agObj : agList) {
                        if('NetJets Europe'.equalsIgnoreCase(agObj.Program__c))
                            NJE_AG_FOUND = TRUE;
                        if('NetJets U.S.'.equalsIgnoreCase(agObj.Program__c))
                            NJUS_AG_FOUND = TRUE;
                    }
    
                    //GCM-9745: Added condition to check current value of flag.
                    if(!NJUS_AG_FOUND && hasActiveNJUSFlag) {
                        accToUpdate.APTS_Has_Active_NJUS_Agreement__c = false;
                        accList.add(accToUpdate);
                    }
                    
                    //GCM-9745: Added condition to check current value of flag.
                    if(!NJE_AG_FOUND && hasActiveNJEFlag) {
                        accToUpdate.APTS_Has_Active_NJE_Agreement__c = false;
                        accList.add(accToUpdate);
                    }
                }
            }
        }

        system.debug('Accounts to be updated --> '+accList.size());
        try {
            // Instead of updating records here, call Queueable Class.
            System.enqueueJob(new APTS_AccountUpdateQueueable(accList));
            processedRecordCount += accList.size();
        } catch(DmlException e) {
            system.debug('Batch Exception while updating records '+e);
        }
        
        
    }
    
    public void finish(Database.BatchableContext BC) {
        //TODO: Discuss if there is need to send an email.
        system.debug('Batch Process has completed and total records processed --> ' + recordCount + 
                     ' Total number of records updated --> '+processedRecordCount);
    }
    
    
}
/************************************************************************************************************************
 @Name: APTS_ConstantUtil
 @Author: Conga PS Dev Team
 @CreateDate:
 @Description: APTS_ConstantUtil. Used to define constants which are used across multilpe classes/triggers
 ************************************************************************************************************************
 @ModifiedBy: Conga PS Dev Team
 @ModifiedDate: 26/11/2020
 @ChangeDescription: Added more constants for Shares and Leases.
 ************************************************************************************************************************/
public class APTS_ConstantUtil{
    public static final string NEW_CARD_SALE_CONVERSION = 'New Card Sale Conversion';
    public static final string DIRECT_CONVERSION = 'Direct Conversion';
    public static final string TERMINATION = 'Termination';
    public static final string TERM_EXTENSION = 'Term Extension';
    public static final string EXPIRED_HOURS_CARD = 'Expired Hours Card';
    public static final string NJ_EUROPE_PROGRAM = 'NetJets Europe';
    public static final string AIRCRAFT_HOURS = 'Aircraft Hours';
    public static final string AIRCRAFT_REMAINING_HOURS = 'Aircraft Remaining Hours';
    public static final string NEW_SALE = 'New Sale';
    public static final string ADD_ENHANCEMENTS = 'Add Enhancements';
    public static final string REDEMPTION_PRODUCT_TYPE = 'Redemption';
    public static final string COMBO_PRODUCT_TYPE = 'Combo';
    public static final string ELITE_COMBO_PRODUCT_TYPE = 'Elite Combo';
    public static final string CLASSIC_COMBO_PRODUCT_TYPE = 'Classic Combo';
    public static final string COMBO_PRODUCT_NJE = 'Inclusive Combo Card';
    public static final string REFEREE_LINE = 'Referee Card';
    public static final string REFERRAL_LINE = 'Referral Card';
    //Program Type
    public static final string NETJETS_USD = 'NetJets U.S.';
    public static final string NETJETS_EUR = 'NetJets Europe';
    //PAV Field Mapping
    public static final string AIRCRAFT_HOURS_PAV = 'Aircraft_Hours__c';
    public static final string PRICE_LIST = 'APTPS_List_Price__c';
    //Lineitem Field Mapping
    public static final string FEDERAL_EXCISE_TAX = 'Federal Excise Tax';
    public static final string HALF_CARD_PREMIUM_FET = 'Half Card Premium FET';
    public static final string PURCHASE_PRICE = 'Purchase Price';
    public static final string LEASE_DEPOSIT = 'Lease Deposit';
    //AgreementLineitem Field Mapping
    public static final string COMBO_CARD_PREMIUM = 'Combo Card Premium';
    public static final string HALF_CARD_PREMIUM = 'Half Card Premium';
    //Assignment
    public static final string ASSIGNMENT = 'Assignment';
    public static final string CROSS_ACCOUNT_ASSIGNMENT = 'Cross-account assignment';
    public static final string INTRA_ACCOUNT_ASSIGNMENT = 'Intra-Account Assignment';
    //Agreement Status
    public static final string AGREEMENT_CONTRACT_GENERATED = 'Contract Generated';
    public static final string AGREEMENT_SALES_REVIEW = 'Sales Review';
    public static final string AGREEMENT_PENDING_FUNDING = 'Pending Funding';
    public static final string AGREEMENT_ACTIVE = 'Active';
    public static final string AGREEMENT_CANCELLED = 'Cancelled';
    public static final string AGREEMENT_CONTRACT_PENDING = 'Contract Pending';
    public static final string AGREEMENT_PENDING_ACTIVATION = 'Pending Activation';
    public static final string AGREEMENT_INACTIVE = 'Inactive';
    public static final string AGREEMENT_TERMINATED = 'Terminated';
    public static final string AGREEMENT_IN_MODIFICATION = 'In Modification';
    public static final string AGREEMENT_AMENDED = 'Amended';
    public static final string AGREEMENT_ON_HOLD = 'On Hold';
    public static final string AGREEMENT_MASTER_AGREEMENT = 'Master Agreement';
    public static final string AGREEMENT_DRAFT = 'Draft';
    public static final string AGREEMENT_IN_REVIEW = 'In Review';
    public static final string AGREEMENT_IN_APPROVALS = 'In Approvals';
    public static final string AGREEMENT_CONTRACT_APPROVED = 'Contract Approved';
    public static final string AGREEMENT_POSITIONING_REQUESTED = 'Positioning Requested';
    public static final string ACCRUAL = 'Accrual';
    //Apttus Status
    public static final string APTTUS_OTHER_PARTY_REVIEW = 'Other Party Review';
    public static final string APTTUS_AUTHER_CONTRACT = 'Author Contract';
    public static final string APTTUS_READY_FOR_SIGNATURE = 'Ready for Signatures';
    
    //Proposal Status
    public static final string APPROVED = 'Approved';
    //Agreement Record Types
    public static final string AGREEMENT_DEFAULT = 'Default';
    public static final string AGREEMENT_MASTER = 'Master';
    public static final string AGREEMENT_MOD = 'Modification';
    public static final string AGREEMENT_NJE_MOD = 'APTS_NJE_Modification';
    public static final string AGREEMENT_NJE_NEW = 'NJE_New_Card_Sale';
    public static final string AGREEMENT_NJUS_LEASE = 'NJUS_Lease_Purchase_Agreement';
    public static final string AGREEMENT_NJUS_MOD = 'APTS_NJUS_Modification';
    public static final string AGREEMENT_NJUS_NEW = 'NJUS_New_Card_Sale';
    public static final string AGREEMENT_NJUS_SHARE = 'NJUS_Share_Purchase_Agreement';
    public static final string AGREEMENT_HOLD = 'On_Hold';
    public static final string INTERIM_AGREEMENT_HOLD= 'APTPS_On_Hold_Interim_Lease';
    public static final string AGREEMENT_PENDING = 'Pending_Agreement';
    public static final string AGREEMENT_REFEREE = 'Referee';
    public static final string AGREEMENT_REFERRER = 'Referrer';
    public static final string AGREEMENT_TERMINATION = 'Termination';
    
    //Deal Summary constants
    public static final string NJUS_DEMO = 'NJUS_DEMO';
    public static final string NJE_DEMO = 'NJE_DEMO';
    public static final string CT_PRODUCT_CODE = 'NJE_Corporate_Trial';
    public static final string LINE_TYPE = 'Product/Service';
    public static final string OPTION = 'Option';
    public static final string AIRCRAFT = 'Aircraft';
    public static final string NETJETS_PRODUCT = 'Netjets Product';
    
    
    //SNL Product names
    public static final string DEMO = 'Demo';
    public static final string CORP_TRIAL = 'Corporate Trial';
    public static final string SHARE = 'Share';
    public static final string LEASE = 'Lease';
    public static final string G450_AIRCRAFT = 'Gulfstream 450';
    public static final string GLOBAL7500_AIRCRAFT = 'Global 7500';
    public static final string SPLIT_BILL_CODE = 'SPLIT_BILLING_NJUS';
    public static final string PRICE_INCENTIVE_CODE = 'PURCHASE_PRICE_INCENTIVES_NJUS';
    public static final string ACC_FEE_CODE = 'ACC_FEES_NJUS';
    public static final string INTERIM_LEASE_CODE = 'INTERIM_LEASE_NJUS';
    public static final string PREPAY_FEE_CODE = 'PREPAYMENT_OF_FEES_NJUS';
    public static final string NJA_SHARE_CODE = 'NJUS_SHARE';
    public static final string NJE_SHARE_CODE = 'NJE_SHARE';
    public static final string NJA_LEASE_CODE = 'NJUS_LEASE';
    public static final string NJE_LEASE_CODE = 'NJE_LEASE';
    public static final string SPLIT_BILL_CODE_NJE = 'SPLIT_BILLING_NJE';
    public static final string PRICE_INCENTIVE_CODE_NJE = 'PURCHASE_PRICE_INCENTIVES_NJE';
    public static final string ACC_FEE_CODE_NJE = 'ACC_FEES_NJE';
    public static final string INTERIM_LEASE_CODE_NJE = 'INTERIM_LEASE_NJE';
    public static final string PREPAY_FEE_CODE_NJE = 'PREPAYMENT_OF_FEES_NJE';
    public static final string MMF_CHARGE_TYPE = 'Monthly Management Fee';
    public static final string MLF_CHARGE_TYPE = 'Monthly Lease Payment';
    public static final string OF_CHARGE_TYPE = 'Operating Fund';
    public static final string OHF_CHARGE_TYPE = 'Occupied Hourly Fee';
     public static final string OHF_CHARGE_TYPE_1 = 'Occupied Hourly Fee 1';
    public static final string OHF_CHARGE_TYPE_2 = 'Occupied Hourly Fee 2';
    public static final string OHF_CHARGE_TYPE_3 = 'Occupied Hourly Fee 3';
    public static final string OHF_CHARGE_TYPE_4 = 'Occupied Hourly Fee 4';
    public static final string FUEL_CHARGE_TYPE = 'Fuel Hourly Rate';
    public static final string SR1_CHARGE_TYPE = 'Supplemental Rate 1';
    public static final string SR2_CHARGE_TYPE = 'Supplemental Rate 2';
    public static final string FERRY_CHARGE_TYPE = 'Ferry Rate';
    public static final string CARD_GRADUATE_CREDIT_NJA = '10_Card_Graduate_Credit_Incentive_NJUS';
    public static final string CARD_GRADUATE_CREDIT_NJE = '10_Card_Graduate_Credit_Incentive_NJE';
    public static final string SNL_NJA_ENHANCEMENT = 'NJUS_ENHANCEMENT';
    public static final string SNL_NJE_ENHANCEMENT = 'NJE_ENHANCEMENT';
    public static final string SNL_ENTITLEMENT_AGR = 'Share/Lease Agreement';
    public static final string SNL_ENTITLEMENT_QUOTE = 'Proposal';
    
    public static final string NJ_SALES_ENTITY = 'NetJets Sales, Inc';
    public static final string NJI_SALES_ENTITY = 'NJI Sales, Inc';
    public static final string NJ_MANAGER_ENTITY= 'Netjets Aviation, Inc';
    public static final string NJI_MANAGER_ENTITY = 'NetJets International Inc';
    public static final string LIGHT_CABIN= 'Light';
    public static final string MID_CABIN= 'Mid-Size';
    public static final string SUPER_MID_CABIN= 'Super-Midsize';
    public static final string LARGE_CABIN = 'Large';

    //SNL - Agreement Lifecycle constants
    public static final string REQUEST = 'Request';
    public static final string IN_SIGN = 'In Signatures';
    public static final string READY_FOR_SIGN = 'Ready for Signatures';
    public static final string OTHER_PARTY_SIGN = 'Other Party Signatures';
    public static final string FULLY_SIGNED = 'Fully Signed';
    public static final string IN_EFFECT = 'In Effect';
    public static final string ACTIVATED = 'Activated';
    public static final string STATUS_ACTIVE = 'Active';
    public static final string PENDING_ACTIVATE = 'Pending Activation';
    public static final string DOC_GENERATED = 'Document Generated';
    public static final string DOC_PENDING = 'Document Pending';
    public static final string FUND_PENDING = 'Pending Funding';
    public static final string FUNDED_CONTRACT = 'Funded Contract';
    public static final string FUNDED_NOTREQUIRED = 'Funding Not Required';
    public static final string STATUS_DRAFT = 'Draft';
    public static final string DOC_SIGNED = 'Document Signed';
    public static final string CUR_USD = 'USD';
    public static final string CUR_EUR = 'EUR';
    public static final string CT_RECORD_TYPE = 'APTPS_NJE_Corporate_Trial';
    public static final string DEMO_NJE_RECORD_TYPE = 'APTPS_NJE_Demo_Agreement';
    public static final string DEMO_NJA_RECORD_TYPE = 'APTPS_NJUS_Demo_Agreement';
    public static final string SHARE_NJA_RECORD_TYPE = 'APTPS_NJA_Share_Purchase_Agreement';
    public static final string SHARE_NJE_RECORD_TYPE = 'APTPS_NJE_Share_Purchase_Agreement';
    public static final string LEASE_NJA_RECORD_TYPE = 'APTPS_NJA_Lease_Purchase_Agreement';
    public static final string LEASE_NJE_RECORD_TYPE = 'APTPS_NJE_Lease_Purchase_Agreement';
    public static final string AGREEMENT_OBJ = 'Apttus__APTS_Agreement__c';
    public static final string IN_AUTH = 'In Authoring';
    public static final string PENDING_POSITIONING= 'Pending Positioning';
    public static final string DOC_RECEIVED = 'Document Received';
    public static final string APPR_REQ = 'Approved Request';
    public static final string APPR_REJ = 'Rejected Approval';
    public static final string AC_POSITIONED = 'Aircraft Positioned';
    public static final string ACCRUAL_AC_POSITIONED = 'Accrual AC Positioned';
    public static final string ACCRUAL_PENDING_ACTIVATION = 'Accrual Pending Activation';
    public static final string ACCRUAL_ACTIVE = 'Accrual Active';
    public static final string CANCEL = 'Cancelled';
    public static final string CANCEL_REQ = 'Cancelled Request';
    public static final string FUNDED_SIGN_PENDING = 'Funded Contract Signature Pending';
    public static final string INTERIM_LEASE_FIELD_SET = 'APTPS_Interim_Lease_Agreement';
    public static final string INTERIM_POSITIONING = 'Interim Positioning';
    public static final string FUNDED_DEPOSIT = 'Funded Deposit';

    //SNL - Quote/Proposal Lifecycle constants
    public static final string APPROVAL_REQUIRED = 'Approval Required';
    public static final string GENERATED = 'Generated';
    public static final string PROPOSAL_SOLUTION = 'Proposal/Solution';
    public static final string PENDING_APPROVAL = 'Pending Approval';
    public static final string IN_REVIEW = 'In Review';
    public static final string IN_APPROVAL = 'In Approval';
    public static final string REJECTED = 'Rejected';
    public static final string QUOTE_DRAFT = 'Draft';
    public static final string QUOTE_OBJ = 'Apttus_Proposal__Proposal__c';
    public static final string NEGOTIATION = 'Contract Negotiation';
    
    //Quote Collaboration constants
    public static final string STATUS_SUBMITTED = 'Submitted';
    public static final string STATUS_COMPLETED = 'Completed';
    public static final string STATUS_ACCEPTED = 'Accepted';
    public static final string STATUS_CANCELLED = 'Cancelled';
    public static final string STATUS_NEW = 'New';
    public static final string COLLAB_REQ_OBJECT_API = 'Apttus_Config2__CollaborationRequest__c';
    public static final string COLLAB_REQ = 'Collaboration Request';
    public static final string PAGE_ACTION = 'Finalize';
}
/************************************************************************************************************************
@Name: APTPS_DealSummary_Share
@Author: Conga PS Dev Team
@CreateDate: 16 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public with sharing class APTPS_DealSummary_Lease {
	/** 
    @description: Read PAVs and prepare deal summary
    @param: Quote Id and Currency Sign
    @return: Final value of Deal Summary
    */
    private static String getSummary(Id qId, String currencySign, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
        String dsText = '';
        Map<Id, String> saNameMap = new Map<Id, String>();
        Map<Id, Double> saAllocatedHrMap = new Map<Id, Double>();
        Map<Id, Double> saPercent = new Map<Id, Double>();
        Double fet, incentiveFee, accFees, leaseDeposit, totalPurchasePrice, cardGraduateCredit;
        totalPurchasePrice = 0;
        fet=incentiveFee=accFees=leaseDeposit=cardGraduateCredit = null;
        Boolean isInterimAvailable, isPrePaymentAvailable;
        isInterimAvailable=isPrePaymentAvailable = false;
        String acName, commitmentPeriod, vintage, hours;
        acName=commitmentPeriod=vintage=hours = null;
        Double percentage = null;
        String delayedStartMonths='';
        String contractLength='';
        Date delayedStartDate, endDate;
        delayedStartDate=endDate = null;
        try{
            for(Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {
                if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && APTS_ConstantUtil.FEDERAL_EXCISE_TAX.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       fet = pli.Apttus_QPConfig__NetPrice__c;
                   }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.ACC_FEE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.ACC_FEE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           accFees = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Fee_Amount__c;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           isInterimAvailable = true;
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.SPLIT_BILL_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.SPLIT_BILL_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           saNameMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__r.Name);
                           saAllocatedHrMap.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Allocated_Hours__c);
                           saPercent.put(pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Service_Account__c, pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percent__c);
                       }
                
                if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PREPAY_FEE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PREPAY_FEE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           isPrePaymentAvailable = true; 
                       }
                
                if(APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
                   && APTS_ConstantUtil.LEASE_DEPOSIT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                       acName = pli.Apttus_QPConfig__OptionId__r.Name;
                       leaseDeposit = pli.Apttus_QPConfig__NetPrice__c;
                       //vintage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Vintage__c;
                       hours = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                       percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                       contractLength = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Contract_Length_Lease__c;
                       delayedStartDate = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_End_Date__c;
                       //commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;
                       endDate = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_End_Date__c;
                       delayedStartMonths = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Delayed_Start_Months__c;
                       
                   }
                
                /*if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJA.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           cardGraduateCredit = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_10_Card_Graduate_Credit_Amount__c;
                       }*/
                
            }
            system.debug('Pricing data, fet --> '+fet+' incentiveFee --> '+incentiveFee+' accFees --> '+accFees
                         +'leaseDeposit --> '+leaseDeposit+' cardGraduateCredit --> '+cardGraduateCredit);
            totalPurchasePrice = leaseDeposit;
            dsText += '<b>Transaction Type:</b> Lease<br/><br/>';
            dsText += '<b>Aircraft Type:</b> '+acName+'<br/><br/>';
            //dsText += '<b>Vintage:</b> '+vintage+'<br/><br/>';
            dsText += '<b>Hours:</b> '+hours+'<br/><br/>';
            dsText += '<b>Share Size:</b> '+hours+' hrs'+' ('+percentage+'%)<br/><br/>';
            if(contractLength !='')
                dsText += '<b>Contract Length:</b> '+contractLength+'<br/><br/>';
            if(endDate != null)
                dsText += '<b>End Date:</b> '+endDate+'<br/><br/>';
            if(delayedStartDate != null)
                dsText += '<b>Delayed Start End Date:</b> '+delayedStartDate+'<br/><br/>';
            if(delayedStartMonths != null)
                dsText += '<b>Delayed Start Months:</b> '+delayedStartMonths+'<br/><br/>';
            //dsText += '<b>Minimum Commitment Period:</b> '+commitmentPeriod+'<br/><br/>';
            //TODO: For now as it's NJUS Share only so adding currency as it is. Add logic(if required) once we implement NJE Share.
            dsText += '<b>Lease Deposit:</b> '+currencySign+leaseDeposit+'<br/><br/>';
            /*if(fet != null && fet > 0) {
                dsText += '<b>FET:</b> '+currencySign+fet+'<br/><br/>';
                totalPurchasePrice += fet;
            }
            
            if(incentiveFee != null && incentiveFee > 0) {
                dsText += '<b>Purchase Price Incentive:</b> '+currencySign+incentiveFee+'<br/><br/>';
                totalPurchasePrice -= incentiveFee;
            }
            
            if(accFees != null && accFees > 0) {
                dsText += '<b>ACC Fees:</b> '+currencySign+accFees+'<br/><br/>';
                totalPurchasePrice += accFees;
            }
            
            if(cardGraduateCredit != null && cardGraduateCredit > 0) {
                dsText += '<b>10% Card Graduate Credit:</b> '+currencySign+cardGraduateCredit+'<br/><br/>';
                totalPurchasePrice -= cardGraduateCredit;
            }
            if(totalPurchasePrice != null && totalPurchasePrice != leaseDeposit) 
                dsText += '<b>Total Purchase Price:</b> '+currencySign+totalPurchasePrice+'<br/><br/>'; */
            dsText += '<b>Options:</b><br/>';
            String optionText = '';
            system.debug('isInterimAvailable --> '+isInterimAvailable+' isPrePaymentAvailable --> '+isPrePaymentAvailable+' Map --> '+saNameMap.isEmpty());
            if(isInterimAvailable || isPrePaymentAvailable || !saNameMap.isEmpty()) {
                optionText = '<ul>';
                if(isInterimAvailable)
                    optionText += '<li>Interim Lease</li>';
                if(isPrePaymentAvailable)
                    optionText += '<li>Prepayment of Fees</li>';
                if(!saNameMap.isEmpty()) {
                    optionText += '<li>Split Billing</li>';
                    for(Id key : saNameMap.keySet()) {
                        String saName = saNameMap.get(key);
                        Double hrs = saAllocatedHrMap.get(key);
                        Double percent = saPercent.get(key);
                        optionText += '<span>Service Account '+saName+' - '+hrs+' hrs ('+percent+'%)</span><br/>';
                    }
                }
                optionText +='</ul>';
            }
            if(!String.isEmpty(optionText))
                dsText += optionText;
            system.debug('Lease Deal Summary --> '+dsText);
        }catch(Exception e){
            system.debug('Exception while computing Deal Summary for Lease NJA. Error details --> '+e.getMessage());
        }
        return dsText;
    }

    /** 
    @description: NJUS Lease implementation
    @param:
    @return: 
    */
    public class NJUS_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJUS deal summary.');
            Id qId = (Id)args.get('qId');
            String dsText = APTPS_DealSummary_Lease.getSummary(qId, '$', quoteLines);
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
    
    /** 
    @description: NJE Share implementation
    @param:
    @return: 
    */
    public class NJE_Summary implements APTPS_DealSummaryInterface {
        public Apttus_Proposal__Proposal__c updateSummary(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Share NJE deal summary.');
            Id qId = (Id)args.get('qId');
            String dsText = APTPS_DealSummary_Lease.getSummary(qId, '€', quoteLines);
            Apttus_Proposal__Proposal__c quoteToUpdate = new Apttus_Proposal__Proposal__c(Id = qId);
            quoteToUpdate.Deal_Description__c = dsText;
            return quoteToUpdate;
        }
    }
}
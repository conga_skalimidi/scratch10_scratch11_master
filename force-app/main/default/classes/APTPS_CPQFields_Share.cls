/************************************************************************************************************************
@Name: APTPS_CPQFields_Share
@Author: Conga PS Dev Team
@CreateDate: 15 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Share{   
    
    public void populateShareFields(Apttus_Proposal__Proposal__c oProposal,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Double operatingFund, MMF, OHF, Fuel, SupplementalRate1,SupplementalRate2,cardGraduateCredit,incentiveFee,contractDeposit,ferryRate,percentage;
        String commitmentPeriod,acName;
        boolean hasInterimLeaseOption = false;
        commitmentPeriod=acName=null;
        cardGraduateCredit=incentiveFee=contractDeposit=ferryRate=percentage=0;
        for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {   
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
               && APTS_ConstantUtil.PURCHASE_PRICE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   commitmentPeriod = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Minimum_Commitment_Period__c;  
                    acName = pli.Apttus_QPConfig__OptionId__r.Name;
                    percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.MMF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   MMF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   operatingFund = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   OHF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.FUEL_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   Fuel = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR1_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate1 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR2_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate2 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.FERRY_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   ferryRate = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJA.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.CARD_GRADUATE_CREDIT_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           cardGraduateCredit = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_10_Card_Graduate_Credit_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           contractDeposit = pli.Apttus_QPConfig__NetPrice__c;
                           hasInterimLeaseOption = true;
            }
        }      
        oProposal.Aircraft_Types__c = acName;
        oProposal.Product_Line__c = APTS_ConstantUtil.SHARE;
        oProposal.APTPS_Minimum_Commitment_Period__c = commitmentPeriod;
        oProposal.APTPS_Contract_Operating_Fund__c = operatingFund;
        oProposal.APTPS_MMF__c = MMF;
        oProposal.APTPS_OHF__c = OHF;
        oProposal.APTPS_Fuel__c = Fuel;
        oProposal.APTPS_Supplemental_Rate1__c = SupplementalRate1;
        oProposal.APTPS_Supplemental_Rate2__c = SupplementalRate2;
        oProposal.APTPS_Purchase_Price_Incentive__c = incentiveFee;
        oProposal.APTPS_10_Card_Graduate_Credit__c = cardGraduateCredit;        
        oProposal.APTPS_Contract_Purchase_Deposit__c = contractDeposit;
        oProposal.APTPS_Ferry_Rate__c = ferryRate;
        oProposal.APTPS_Has_Interim_Lease_Option__c = hasInterimLeaseOption;
        oProposal.APTPS_Share_Percentage__c = percentage;
        
        
         system.debug('oProposal-->'+oProposal);
    }   
   
    /** 
    @description: Demo NJE implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJE Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Share obj = new APTPS_CPQFields_Share();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
    
     /** 
    @description: Demo NJA implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Demo NJA Fields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Share obj = new APTPS_CPQFields_Share();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
}
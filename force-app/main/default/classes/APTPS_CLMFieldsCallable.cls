/************************************************************************************************************************
@Name: APTPS_CLMFieldsCallable
@Author: Conga PS Dev Team
@CreateDate: 28 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMFieldsCallable {
    Object call(Map<String, Object> args);
}
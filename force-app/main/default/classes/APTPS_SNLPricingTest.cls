/************************************************************************************************************************
@Name: APTPS_SNLPricingTest
@Author: Conga PS Dev Team
@CreateDate: 8 July 2021
@Description: Test coverage for SNL custom pricing logic i.e. test coverage for SNL related logic in Pricing Callback.
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_SNLPricingTest {
    @TestSetup
    static void makeData(){
        // Create Settigs
        Apttus_Config2__ConfigCustomClasses__c configCustomClasses = new Apttus_Config2__ConfigCustomClasses__c();
        configCustomClasses.Name = 'Custom Classes';
        configCustomClasses.Apttus_Config2__PricingCallbackClass__c = 'APTPS_PricingCallback';
        insert configCustomClasses;
        
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial,Share';
        insert snlProducts;
        
        insert APTS_CPQTestUtility.getConfigLineItemFields();
        
        Apttus_Config2__ConfigSystemProperties__c configSystemProperties = new Apttus_Config2__ConfigSystemProperties__c();
        configSystemProperties.Name = 'System Properties';
        configSystemProperties.Apttus_Config2__PricingProfile__c = 'Advanced';
        configSystemProperties.Apttus_Config2__PricingBatchSize__c = 2;
        configSystemProperties.Apttus_Config2__OptionPricingChunkSize__c = 100;
        insert configSystemProperties;
        
        APTPS_Pricing_Callback__c fetDetails = new APTPS_Pricing_Callback__c();
        fetDetails.FET__c = 0.075;
        fetDetails.APTPS_SNL_MMF_Price_Factor__c = 50.00;
        insert fetDetails;
        
        Account testAccount = APTS_CPQTestUtility.createAccount('Test SNL PCB', 'Other');
        insert testAccount;
        
        Contact testContact = APTS_CPQTestUtility.createContact('ContactFN', 'PCB Contact', testAccount.Id);
        insert testContact;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('New Sale Share', 'New', testAccount.Id, 'Closed Won');
        insert testOpportunity;
        
        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', testAccount.Id);
        insert testAccLocation;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('USA', true);
        insert testPriceList;
        
        list<Product2> lstProducts = new list<Product2>();
        
        // Insert Share Bundle (US)
        Product2 testShareProduct = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Netjets Product', 'Bundle', true, true, true, true);
        testShareProduct.Program__c = 'NetJets U.S.';
        
        // Insert Aircraft Option (US)
        Product2 testAircraft1Product = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Aircraft', 'Option', true, true, true, true);
        testAircraft1Product.Program__c = 'NetJets U.S.';
        testAircraft1Product.Cabin_Class__c = 'Light';
        
        lstProducts.add(testShareProduct);
        lstProducts.add(testAircraft1Product);
        insert lstProducts;
        
        Apttus_Config2__ClassificationName__c categorySo = APTS_CPQTestUtility.createCategory('Netjets Products', 'Netjets Products', 'Offering', null, true);
        insert categorySo;
        
        list<Apttus_Config2__PriceListItem__c> lstPLI = new list<Apttus_Config2__PriceListItem__c>();
        //Share PLI
        Apttus_Config2__PriceListItem__c mmfPLI = APTS_CPQTestUtility.createPriceListItem(testPriceList.Id, testShareProduct.Id, 9250.00, APTS_ConstantUtil.MMF_CHARGE_TYPE, 'One Time', 'Per Unit', 'Each', true);
        lstPLI.add(mmfPLI);
        insert lstPLI;
        
        Apttus_Proposal__Proposal__c nsProposal = APTS_CPQTestUtility.createProposal('Share Quote/Proposal', testAccount.Id, testOpportunity.Id, 'Proposal NJUS', testPriceList.Id);
        nsProposal.APTS_New_Sale_or_Modification__c = 'New Sale';
        nsProposal.APTPS_Program_Type__c = 'Share';
        nsProposal.CurrencyIsoCode = 'USD';
        insert nsProposal;
        
        // Add Configuration
        Id configuration = APTS_CPQTestUtility.createConfiguration(nsProposal.Id);
        
        List<Apttus_Config2__LineItem__c> lineitemList = new List<Apttus_Config2__LineItem__c>();
        Apttus_Config2__LineItem__c mmfLineItem = APTS_CPQTestUtility.getLineItem(configuration, nsProposal, mmfPLI, testShareProduct, testShareProduct.Id, testAircraft1Product.Id, 'Product/Service', 1, 1, 1, 0, null, 1);
        mmfLineItem.Apttus_Config2__PricingStatus__c = 'Pending';
        lineitemList.add(mmfLineItem);
        insert lineitemList;
        
        List<Apttus_Config2__ProductAttributeValue__c> sharePAVList = new List<Apttus_Config2__ProductAttributeValue__c>();
        Apttus_Config2__ProductAttributeValue__c sharePAV = new Apttus_Config2__ProductAttributeValue__c();
        sharePAV.APTPS_Hours__c = '50';
        sharePAV.Apttus_Config2__LineItemId__c = mmfLineItem.Id;
        sharePAVList.add(sharePAV);
        insert sharePAVList;	
    }
    
    @isTest
    public static void testMMF() {
        Test.startTest();
        Apttus_Proposal__Proposal__c shareQuote = [SELECT ID FROM Apttus_Proposal__Proposal__c WHERE CurrencyIsoCode = 'USD' AND APTPS_Program_Type__c = 'Share' LIMIT 1];
        List<Apttus_Config2__ProductConfiguration__c> config = [SELECT ID FROM Apttus_Config2__ProductConfiguration__c 
                                                                WHERE Apttus_QPConfig__Proposald__c =:shareQuote.Id LIMIT 1];
        Apttus_CpqApi.CPQ.UpdatePriceRequestDO objUpdatePriceRequestDO = new Apttus_CpqApi.CPQ.UpdatePriceRequestDO();
        objUpdatePriceRequestDO.CartId = config[0].Id;
        System.debug('==objUpdatePriceRequestDO==' + objUpdatePriceRequestDO);
        Apttus_CpqApi.CPQ.UpdatePriceResponseDO result = Apttus_CpqApi.CPQWebService.updatePriceForCart(objUpdatePriceRequestDO);
        System.debug('==result==' + result);
        
        Apttus_Config2.PricingWebService.computeNetPriceForItemColl(config[0].Id, 1);
        Test.stopTest();
    }
}
public class APTS_AssetLineItemBaseTriggerHandler extends TriggerHandler {	
    
    public override void afterUpdate() {
        List<Apttus_Config2__AssetLineItem__c> newAssetLIs = Trigger.New;
         system.debug('>>>>>>>>>>afterUpdate - newAssetLIs>>>>>> ' + newAssetLIs);
         APTS_AssetLineItemBaseTriggerHelper.updateEHCOnParentAssetPAV(newAssetLIs);
       
    }
    
    
}
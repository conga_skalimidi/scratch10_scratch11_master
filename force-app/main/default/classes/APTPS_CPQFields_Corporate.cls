/************************************************************************************************************************
@Name: APTPS_CPQFields_Corporate
@Author: Conga PS Dev Team
@CreateDate: 09 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Corporate{   
    
    public void populateCTFields(Apttus_Proposal__Proposal__c proposalObj,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Apttus_Proposal__Proposal_Line_Item__c oPLI = null;
         Id CTRecTypeId = Schema.SObjectType.Apttus_Proposal__Proposal__c.getRecordTypeInfosByDeveloperName().get('APTPS_NJE_Corporate_Trial_Proposal').getRecordTypeId();
        for (Apttus_Proposal__Proposal_Line_Item__c proposalLineItem : quoteLines) {                    
            if(APTS_ConstantUtil.LINE_TYPE.equalsIgnoreCase(proposalLineItem.Apttus_QPConfig__LineType__c)) {
                oPLI = proposalLineItem;
                break;
            }
        }
        if(oPLI != null) {
            proposalObj.APTPS_Term_End_Date__c = oPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_End_Date__c;
            proposalObj.APTPS_Term_Months__c = oPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Term_Months__c;
            proposalObj.APTPS_Override_Deposit__c = oPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Override_Deposit__c;
            proposalObj.Aircraft_Types__c = oPLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Corporate_Trial_Aircraft__c;
            proposalObj.RecordTypeId = CTRecTypeId;
            proposalObj.Product_Line__c = APTS_ConstantUtil.CORP_TRIAL;
        }
         system.debug('proposalObj-->'+proposalObj);
    }   
   
    /** 
    @description: Coprporate Trail implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Corporate NJE dApprovals');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Corporate obj = new APTPS_CPQFields_Corporate();
            obj.populateCTFields(proposalObj,quoteLines);            
            
        }
    }
}
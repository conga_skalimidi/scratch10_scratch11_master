/************************************************************************************************************************
@Name: APTPS_CLM_AutomationSchedulable
@Author: Conga PS Dev Team
@CreateDate: 24 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLM_AutomationSchedulable implements Schedulable {
    Map<Id, Id> agToActivate = new Map<Id, Id>();
    Map<Id, Id> quoteDocMap = new Map<Id, Id>();
    
    public APTPS_CLM_AutomationSchedulable(Map<Id, Id> agToActivate, Map<Id, Id> quoteDocMap) {
        this.agToActivate = agToActivate;
        this.quoteDocMap = quoteDocMap;
    }
    
    public void execute(SchedulableContext sc) {
        //TODO: Check-should we add executed/created by condition. This is will help to reduce the time wait.
        Integer jobs = [SELECT count() FROM AsyncApexJob 
                        WHERE (ApexClass.name = 'CopyCartAttributeQJob' OR ApexClass.name = 'CopyQuoteAttributeQJob' 
                               OR ApexClass.name = 'CopyCartUsageTierQJob' OR ApexClass.name = 'CopyQuoteUsageTierQJob') 
                        AND (Status = 'Queued' or Status = 'Processing' or Status = 'Preparing')];
        if(jobs == 0) {
            //Activate Agreements
            APTPS_Demo_Agreement_Automation demoAutoObj = new APTPS_Demo_Agreement_Automation();
            system.debug('Activating these agreements --> '+agToActivate);
            Map<Id, Id> agDocMap = new Map<Id, Id>();
            for(Attachment doc : [SELECT Id, ParentId, Name FROM Attachment WHERE ParentId IN :agToActivate.values()]){
                agDocMap.put(doc.ParentId, doc.Id);
            }
            for(Id qId : agToActivate.keySet()) {
                if(!agDocMap.isEmpty() && agDocMap.containsKey(agToActivate.get(qId)))
                    demoAutoObj.activateAgreement(agToActivate.get(qId), agDocMap.get(agToActivate.get(qId)));
            }
        } else{
            //TODO: Read this time from custom settings if required.
            String nextExecutionTime = Datetime.now().addSeconds(5).format('s m H d M ? yyyy');  
            System.schedule('ScheduledJob ' + String.valueOf(Math.random()), nextExecutionTime, 
                            new APTPS_CLM_AutomationSchedulable(agToActivate, quoteDocMap));
        }
    }
}
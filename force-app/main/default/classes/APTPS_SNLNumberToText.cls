public class APTPS_SNLNumberToText {
	public  static String populateAgreementExtNumberToTextFields(String numberValue){
        String spText = null;
        Map<String,APTPS_Lease_Number_to_Text__mdt> sp = APTPS_Lease_Number_to_Text__mdt.getAll();
        for(String spRec: sp.keySet()){
        if((sp.get(spRec).MasterLabel)==numberValue){
            spText = sp.get(spRec).Lease_Number_Text__c;  
            return spText;
        }
        }  
        return spText;
    }
}
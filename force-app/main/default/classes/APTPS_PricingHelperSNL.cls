/************************************************************************************************************************
@Name: APTPS_PricingHelperSNL
@Author: Conga PS Dev Team
@CreateDate: 09 Nov 2021
@Description: Helper class for SNL Pricing
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_PricingHelperSNL {
    /**
     @description: Stamp OHF flags on Line Items in case of SNL Pricing
     @param: item - Line Item
     @param: ohfListPrice - List Price
	 @param: applicableOHFList - List of applicable OHF charge types
     @return: Computed MMF price
     */
    public static boolean stampOHFDetails(Apttus_Config2__LineItem__c item, Decimal ohfListPrice, List<String> applicableOHFList, Decimal ohfLimits) {
        boolean isValidOHFFound = false;
        system.debug('ohfLimits --> '+ohfLimits);
        Decimal ohfNA = ohfLimits*-1;
        system.debug('ohfNA --> '+ohfNA);
        if(ohfListPrice != null && ohfListPrice != ohfNA && ohfListPrice != ohfLimits) {   
            item.APTPS_Is_TBD__c = false;
            item.APTPS_Is_NA__c = false;
            isValidOHFFound = true;
        }
        
        if(ohfListPrice != null && ohfListPrice == ohfNA) {
            item.APTPS_Is_TBD__c = false;
            item.APTPS_Is_NA__c = true;
        }
        
        if(ohfListPrice != null && ohfListPrice == ohfLimits) {
            item.APTPS_Is_TBD__c = true;
            item.APTPS_Is_NA__c = false;
        }
        
        item.APTPS_Is_Included_In_OHF__c = (applicableOHFList != null && applicableOHFList.contains(item.Apttus_Config2__ChargeType__c)) ? true : false;
        system.debug('stampOHFDetails --> isValidOHFFound, '+isValidOHFFound);
        if(!isValidOHFFound)
            item.Apttus_Config2__BasePrice__c = 0;
        return isValidOHFFound;
    }
    
    /**
     @description: Compute the MMF price for Share
     @param: item - Line Item
     @param: listPrice - List Price
     @return: Computed MMF price
     */
    public static Decimal computeMMFPrice(Apttus_Config2__LineItem__c item, Decimal listPrice, Decimal definedPriceFactor) {
        Decimal computedMMFPrice = 0;
        try{
            Decimal shareHours = item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null ? Decimal.valueOf(item.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c) : 0;
            Decimal mmfPriceFactor = shareHours/definedPriceFactor; //We are not adding validation to check for zero, instead execution should fail.
            Decimal mmfListPrice = listPrice != null ? listPrice : 0;
            system.debug('definedPriceFactor --> '+definedPriceFactor+'shareHours --> '+shareHours+' mmfPriceFactor --> '+mmfPriceFactor+' mmfListPrice --> '+mmfListPrice);
            computedMMFPrice = mmfPriceFactor*mmfListPrice;
        } catch(Exception e) {
            system.debug('Error while computing MMF Price. Error details --> ' + e + ' Line Number-->' + e.getLineNumber());
        }

        system.debug('computeMMFPrice, MMF computed Price is --> '+computedMMFPrice);
        return computedMMFPrice;
    }
}
/*************************************************************
@Name: APTS_CalculationFromAssetLineItems
@Author: Anjali Kumari
@CreateDate: 2 June 2020
@Description : This Action class is called when asset line item is created to calulate total hours of lease/share products at account level
******************************************************************/
global class APTS_CalculationFromAssetLineItems {
    
    @InvocableMethod(label='CalculateTotalHoursFromAssetLines')
    public static void calculate(List<Id> accountIds) {
        try{
            system.debug('accountIds->'+accountIds);
            Integer totalHours = 0;
            //Can't use aggregate quey because APTPS_Hours__c is a picklist.
            for(Apttus_Config2__AssetLineItem__c asset : [SELECT id,Apttus_Config2__AttributeValueId__c,Apttus_Config2__AttributeValueId__r.APTPS_Hours__c 
                                                          FROM Apttus_Config2__AssetLineItem__c 
                                                          WHERE Apttus_Config2__ShipToAccountId__c IN :accountIds 
                                                          AND Apttus_Config2__ProductId__r.Family ='Netjets Product' 
                                                          AND Apttus_Config2__ChargeType__c ='Supplemental Rate 1' 
                                                          AND (Apttus_Config2__ProductId__r.Name='Share' OR Apttus_Config2__ProductId__r.Name='Lease' ) 
                                                          AND Apttus_Config2__LineType__c ='Option']){
                                                              if(asset.Apttus_Config2__AttributeValueId__c != null && asset.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null)
                                                                  totalHours = totalHours + Integer.valueOf(asset.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c);
                                                          }
            system.debug('totalHours ->'+totalHours );
            Account acc = [SELECT APTPS_Aicraft_Hours_For_Share_Lease__c FROM Account WHERE id =:accountIds[0]];
            acc.APTPS_Aicraft_Hours_For_Share_Lease__c = totalHours;
            update acc;
        }catch(Exception e){
            System.debug('Exception in APTS_CalculationFromAssetLineItems class: Message=:'+e.getMessage()+' at Line no-:'+e.getLineNumber());
        }
        
    }
    
    
}
/*
 * Class Name: APTS_QuoteProposalUtility
 * Description: APTS_QuoteProposalUtility used for utility methods related to Quote/Proposal Object
 *  1. updateChangedEnhancementsOnProposal method used to populate fields to track changed enhancements which will trigger approvals
 *  2. GCM-7030 : Stamp Business Development Hours on Proposal.
 *  3. GCM-7468 : Stamp Bonus Hours and Business Development Hours for NJE
 *  4. GCM-8314 : Stamp Bonus Hours and Business Development Hours in case of New Sale Modification Type.
 *  5. GCM-8270 :  Bonus hour should be on small cabin calss rank attribute value on combo card.
 * Created By: DEV Team, Apttus

 * Modification Log
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Developer               Date           US/Defect        Description
 * ------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Yash Modi             21/11/19           -                Added
 * Avinash Bamane        05/02/20         GCM-7030           Added
 * Avinash Bamane        07/04/20         GCM-7468           Added
 * Avinash Bamane        16/07/20         GCM-8314           Updated
 * Bijal                 28/07/20         GCM-8289           Updated
 * Siva                  03/09/20         GCM-8270           Updated
 * Siva Kumar            18/05/21         -                  Added
*/

global class APTS_QuoteProposalUtility {

    /**
    * Method is used in APTS_ValidationCallback
    * @param cart - used in Validation Callback
    * @return null
    */
   
    @future(callout=true)
    public static void updateChangedEnhancementsOnProposal (Id cartId, String modificationType) { //GCM-8314: New input parameter - modificationType
        system.debug('Modification type --> '+modificationType);
        // Query Enhancement Line Item related to cart
        List <Apttus_Config2__LineItem__c> listLineItems = [
            
            Select Apttus_Config2__LineStatus__c,
                Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c,
                Apttus_Config2__AttributeValueId__r.Death_or_Disability_Clause__c,
                Apttus_Config2__AttributeValueId__r.Business_Development_Partnership__c,
                Apttus_Config2__AttributeValueId__r.APTS_Referred_Prospect_Hours__c,
                Apttus_Config2__AttributeValueId__r.APTS_Bonus_Hours__c,
                Apttus_Config2__AttributeValueId__r.Amount__c, //GCM-7030
                Apttus_Config2__AttributeValueId__r.Non_Standard__c,
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.Death_or_Disability_Clause__c,
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.Business_Development_Partnership__c,
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.APTS_Referred_Prospect_Hours__c,
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.APTS_Bonus_Hours__c,
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.Amount__c, //GCM-7030
                Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r.Non_Standard__c,
                Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c   
            From Apttus_Config2__LineItem__c
            Where Apttus_Config2__ConfigurationId__c = :cartId
                AND (Apttus_Config2__OptionId__r.ProductCode = 'Enhancements' OR Apttus_Config2__OptionId__r.ProductCode = 'Enhancements_NJE') //GCM-7468: Added OR condition
                AND (Apttus_Config2__ProductId__r.ProductCode = 'NJUS_CARD' OR Apttus_Config2__ProductId__r.ProductCode = 'NJE_CARD')
        ];
        
        Apttus_Config2__LineItem__c oLineItem;
        
        if (!listLineItems.isEmpty ()) {
            //START: GCM-8314, condition added to select New Enhancement Line Item.
            if('New Card Sale Conversion'.equalsIgnoreCase(modificationType)) {
                for(Apttus_Config2__LineItem__c li : listLineItems) {
                    if(li.Apttus_Config2__LineStatus__c != null && 
                       'New'.equalsIgnoreCase(li.Apttus_Config2__LineStatus__c)) {
                            oLineItem = li;
                            break;
                       }
                }
            } else // END: GCM-8314
                oLineItem = listLineItems.get (0);
        }
        
        System.debug ('oLineItem: ' + oLineItem);
        
        if (oLineItem != null) {

            Apttus_Config2__ProductAttributeValue__c oPAV = oLineItem.Apttus_Config2__AttributeValueId__r;

            Apttus_Proposal__Proposal__c oProposal = new Apttus_Proposal__Proposal__c (Id = oLineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c);

            // Compare Attributes if Asset Line Item exists otherwise check for new enhancements and updated fields on proposal
            /*if(oLineItem.Apttus_Config2__AssetLineItemId__r != null && oLineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c!='Add Enhancements') {
                
                 oProposal.APTS_Active_Non_Standard__c = oPAV.Non_Standard__c != null && oAAV.Non_Standard__c == null;
            }*/
            if (oLineItem.Apttus_Config2__AssetLineItemId__r != null) {
                Apttus_Config2__AssetAttributeValue__c oAAV = oLineItem.Apttus_Config2__AssetLineItemId__r.Apttus_Config2__AttributeValueId__r;
                
                oProposal.APTS_Active_Death_or_Disability__c = oPAV.Death_or_Disability_Clause__c != oAAV.Death_or_Disability_Clause__c
                                                                    && oPAV.Death_or_Disability_Clause__c;

                oProposal.APTS_Active_Business_Dev_Partnership__c = oPAV.Business_Development_Partnership__c != null
                                                                    && oAAV.Business_Development_Partnership__c == null;

                oProposal.APTS_Active_Card_Referral__c = oPAV.APTS_Referred_Prospect_Hours__c > 0 
                                                                    && (oAAV.APTS_Referred_Prospect_Hours__c == null 
                                                                        || oAAV.APTS_Referred_Prospect_Hours__c == 0);

                oProposal.APTS_Updated_Bonus_Hous__c = Math.abs ((oPAV.APTS_Bonus_Hours__c != null ? oPAV.APTS_Bonus_Hours__c : 0)
                                                            - (oAAV.APTS_Bonus_Hours__c != null ? oAAV.APTS_Bonus_Hours__c : 0)).intValue ();

                if(oLineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__r.APTS_Modification_Type__c!='Add Enhancements') {
                    oProposal.APTS_Active_Non_Standard__c = oPAV.Non_Standard__c != null && oAAV.Non_Standard__c != null && oPAV.Non_Standard__c != oAAV.Non_Standard__c;
                    system.debug('In If--> '+oPAV.Non_Standard__c+' '+oAAV.Non_Standard__c);
                } else {
                    if(oAAV.Non_Standard__c !=null && oPAV.Non_Standard__c != null && oPAV.Non_Standard__c!=oAAV.Non_Standard__c) {
                        oProposal.APTS_Active_Non_Standard__c = true;
                    } else {
                        oProposal.APTS_Active_Non_Standard__c = oPAV.Non_Standard__c != null && oAAV.Non_Standard__c == null;
                    }
                    
                    system.debug('In else--> '+oPAV.Non_Standard__c+' '+oAAV.Non_Standard__c);
                }
                
                // GCM-7030: Start
                oProposal.APTS_Business_Development_Hours__c = oPAV.Amount__c != null ? oPAV.Amount__c : 0;
                // GCM-7030: End
            } else {
                
                oProposal.APTS_Active_Death_or_Disability__c = oPAV.Death_or_Disability_Clause__c;

                oProposal.APTS_Active_Business_Dev_Partnership__c = oPAV.Business_Development_Partnership__c != null;

                oProposal.APTS_Active_Card_Referral__c = oPAV.APTS_Referred_Prospect_Hours__c > 0;

                oProposal.APTS_Updated_Bonus_Hous__c = Math.abs (oPAV.APTS_Bonus_Hours__c != null ? oPAV.APTS_Bonus_Hours__c : 0).intValue ();

                oProposal.APTS_Active_Non_Standard__c = oPAV.Non_Standard__c != null;
                // GCM-7030: Start
                oProposal.APTS_Business_Development_Hours__c = oPAV.Amount__c != null ? oPAV.Amount__c : 0;
                // GCM-7030: End
            }
            System.debug ('oProposal: ' + oProposal);
            //GCM-8270 Start
            integer aircraftCount = 0;
            Decimal cabinClassRank = 0;
            map<Decimal,id> rankAttributeMap = new map<Decimal,id>();
            Apttus_Config2__ProductAttributeValue__c updateLIAttribute = new Apttus_Config2__ProductAttributeValue__c();
            List<Apttus_Config2__ProductAttributeValue__c> liAttributeList = new List<Apttus_Config2__ProductAttributeValue__c>();
            for(Apttus_Config2__LineItem__c li : [SELECT id,Apttus_Config2__OptionId__r.Cabin_Class_Rank__c,Apttus_Config2__ChargeType__c,Apttus_Config2__AttributeValueId__c  
                                                FROM Apttus_Config2__LineItem__c 
                                                WHERE Apttus_Config2__OptionId__r.Family = 'Aircraft' 
                                                AND Apttus_Config2__ChargeType__c = 'Purchase Price' 
                                                AND Apttus_Config2__ConfigurationId__c =: cartId]){
                                                    
                rankAttributeMap.put(li.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c,li.Apttus_Config2__AttributeValueId__c);
                if( li.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c > cabinClassRank)
                    cabinClassRank = li.Apttus_Config2__OptionId__r.Cabin_Class_Rank__c;
                aircraftCount++;
                
            }
            if(aircraftCount > 1) {
                if(rankAttributeMap.containsKey(cabinClassRank)){
                    Id liAttributeId = rankAttributeMap.get(cabinClassRank);
                    updateLIAttribute.id = liAttributeId;
                    updateLIAttribute.Amount__c = null;
                    updateLIAttribute.APTS_Bonus_Hours__c = null;
                    liAttributeList.add(updateLIAttribute);
                }
            }
            system.debug('aircraftCount-->'+aircraftCount+' rankAttributeMap-->'+rankAttributeMap+' cabinClassRank-->'+cabinClassRank+' updateLIAttribute-->'+updateLIAttribute);
            //GCM-8270 End
            try { 
                if(!liAttributeList.isEmpty()){
                    update liAttributeList;
                }
                update oProposal;
            } catch (DmlException e) {
                System.debug (e);
            }

        }
        
        
        
        

    }
     /**
    * Method is used in APTS_ValidationCallback
    * @param cart - used in Validation Callback
    * @return null
    *@Author Siva Kumar
    */
   
    @future(callout=true)
    public static void updateCTAttributeFieldsOnProposal (Id cartId) {

        // Query Enhancement Line Item related to cart
        List <Apttus_Config2__LineItem__c> listLineItems = [
            
            Select Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c, 
                Apttus_Config2__LineType__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Term_End_Date__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Term_Months__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Override_Deposit__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Corporate_Trial_Aircraft__c,
                Apttus_Config2__OptionId__r.ProductCode 
                
            From Apttus_Config2__LineItem__c
            Where Apttus_Config2__ConfigurationId__c = :cartId
                AND Apttus_Config2__ProductId__r.ProductCode = 'NJE_Corporate_Trial'
        ];
        
        Apttus_Config2__LineItem__c oLi;        
        if (!listLineItems.isEmpty ()) oLi = listLineItems.get (0);        
        System.debug ('oLi: ' + oLi);        
        if (oLi != null) {
            Apttus_Proposal__Proposal__c oProposal = new Apttus_Proposal__Proposal__c (Id = oLi.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c);
            oProposal.APTPS_Term_End_Date__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Term_End_Date__c;
            oProposal.APTPS_Term_Months__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Term_Months__c;
            oProposal.APTPS_Override_Deposit__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Override_Deposit__c;
            //oProposal.APTPS_Corporate_Trial_Aircraft__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Corporate_Trial_Aircraft__c;
            oProposal.Aircraft_Types__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Corporate_Trial_Aircraft__c;
            oProposal.Product_Line__c = APTS_ConstantUtil.CORP_TRIAL;
            System.debug ('oProposal: ' + oProposal);
            try { 
                update oProposal;
            } catch (DmlException e) {
                System.debug (e);
            }
        }
    }

    
    
    /**
    * Method is used in APTS_ValidationCallback
    * @param cart - used in Validation Callback
    * @return null
    *@Author Siva Kumar
    */
   
    @future(callout=true)
    public static void updateDemoAttributeFieldsOnProposal (Id cartId) {

        // Query Enhancement Line Item related to cart
        List <Apttus_Config2__LineItem__c> listLineItems = [
            
            Select Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c, 
                Apttus_Config2__LineType__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Type_of_Demo__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Purpose_of_Flight__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Flying_with_Competition__c,
                Apttus_Config2__AttributeValueId__r.APTPS_How_do_they_currently_fly__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Demo_Aircraft__c,
                Apttus_Config2__AttributeValueId__r.APTPS_NJE_Demo_Aircraft__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Date__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Time__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Departure_Airport_Code__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Arrival_Airport_Code__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Passengers__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Pets__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Payment_Method__c,
                Apttus_Config2__AttributeValueId__r.APTPS_NJE_Payment_Method__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Waive_Credit_Card_Authorization__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Comments__c,                
                Apttus_Config2__AttributeValueId__r.APTPS_NJE_Rate_Type__c,                
                Apttus_Config2__AttributeValueId__r.APTPS_Rate_Type__c,                
                Apttus_Config2__AttributeValueId__r.APTPS_Waive_Positioning_Fees__c,               
                Apttus_Config2__OptionId__r.ProductCode, 
                Apttus_Config2__ProductId__r.ProductCode 
                
            From Apttus_Config2__LineItem__c
            Where Apttus_Config2__ConfigurationId__c = :cartId
                AND (Apttus_Config2__ProductId__r.ProductCode = 'NJE_DEMO' OR Apttus_Config2__ProductId__r.ProductCode = 'NJUS_DEMO')
        ];
        
        if (!listLineItems.isEmpty ()) {

            Apttus_Proposal__Proposal__c oProposal = new Apttus_Proposal__Proposal__c (Id = listLineItems[0].Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c);
            for(Apttus_Config2__LineItem__c oLi : listLineItems) {
                if(oLi.Apttus_Config2__LineType__c == 'Product/Service') {
                    if(oLi.Apttus_Config2__ProductId__r.ProductCode == 'NJE_DEMO') {
                        
                       // oProposal.APTPS_DemoAircraft__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_NJE_Demo_Aircraft__c;
                        oProposal.Aircraft_Types__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_NJE_Demo_Aircraft__c;
                        oProposal.APTPS_PaymentMethod__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_NJE_Payment_Method__c; 
                        oProposal.APTPS_NJE_Rate_Type__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_NJE_Rate_Type__c; 
                    } else if(oLi.Apttus_Config2__ProductId__r.ProductCode == 'NJUS_DEMO') {
                        //oProposal.APTPS_DemoAircraft__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Demo_Aircraft__c;
                        oProposal.Aircraft_Types__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Demo_Aircraft__c;
                        oProposal.APTPS_PaymentMethod__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Payment_Method__c; 
                        oProposal.APTPS_Rate_Type__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Rate_Type__c; 
                    }
                    
                    oProposal.APTPS_Type_of_Demo__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Type_of_Demo__c;
                    oProposal.APTPS_Purpose_of_Flight__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Purpose_of_Flight__c;
                    oProposal.APTPS_Flying_with_Competition__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Flying_with_Competition__c;
                    oProposal.APTPS_How_do_they_currently_fly__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_How_do_they_currently_fly__c;
                                       
                    oProposal.APTPS_Comments__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Comments__c;                
                    if(oLi.Apttus_Config2__AttributeValueId__r.APTPS_Waive_Credit_Card_Authorization__c) {
                        oProposal.APTPS_Waive_Credit_Card_Authorization__c = 'Yes'; 
                    } else {
                        oProposal.APTPS_Waive_Credit_Card_Authorization__c = 'No';
                    }
                                     
                    oProposal.APTPS_Waive_Positioning_Fees__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Waive_Positioning_Fees__c;                  
                }else if(oLi.Apttus_Config2__LineType__c == 'Option') {
                    if(oLi.Apttus_Config2__OptionId__r.ProductCode == 'NJE_FLIGHT_INFO' || oLi.Apttus_Config2__OptionId__r.ProductCode == 'NJUS_FLIGHT_INFO') {
                        oProposal.APTPS_Date__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Date__c;
                        oProposal.APTPS_Time__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Time__c;
                        oProposal.APTPS_Departure_Airport_Code__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Departure_Airport_Code__c;
                        oProposal.APTPS_Arrival_Airport_Code__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Arrival_Airport_Code__c;
                        oProposal.APTPS_Number_of_Passengers__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Passengers__c;
                        oProposal.APTPS_Number_of_Pets__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Pets__c;
                    }
                    if(oLi.Apttus_Config2__OptionId__r.ProductCode == 'NJUS_RET_FLIGHT_INFO' || oLi.Apttus_Config2__OptionId__r.ProductCode == 'NJE_RET_FLIGHT_INFO') {
                        oProposal.APTPS_Return_Arrival_Airport_Code__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Arrival_Airport_Code__c;
                        oProposal.APTPS_Return_Date__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Date__c;
                        oProposal.APTPS_Return_Departure_Airport_Code__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Departure_Airport_Code__c;
                        oProposal.APTPS_Return_Number_of_Pets__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Pets__c;
                        oProposal.APTPS_Return_Number_of_Passengers__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Number_of_Passengers__c;
                        oProposal.APTPS_Return_Time__c = oLi.Apttus_Config2__AttributeValueId__r.APTPS_Time__c;
                        oProposal.APTPS_Return_Flight_Information__c = true;
                    } else {
                        oProposal.APTPS_Return_Arrival_Airport_Code__c = null;
                        oProposal.APTPS_Return_Date__c = null;
                        oProposal.APTPS_Return_Departure_Airport_Code__c = null;
                        oProposal.APTPS_Return_Number_of_Pets__c = null;
                        oProposal.APTPS_Return_Number_of_Passengers__c = null;
                        oProposal.APTPS_Return_Time__c = null;
                        oProposal.APTPS_Return_Flight_Information__c = false;
                    }
                }
            }
            
                
               
            oProposal.Product_Line__c = APTS_ConstantUtil.DEMO;
            System.debug ('oProposal: ' + oProposal);
            try { 
                update oProposal;
            } catch (DmlException e) {
                System.debug (e);
            }

        }

    }

    //GCM-8289 Start
    @future(callout=true)
    public static void checkUpgradeEligible (List<Id> listLineItemId, String modificationType){
        
        Apttus_Config2__LineItem__c qsExecLineItem = new Apttus_Config2__LineItem__c();
        List<Apttus_Config2__LineItem__c>  qsExecLineItemList = new List<Apttus_Config2__LineItem__c>();
        
        if(modificationType == 'Assignment') {
            qsExecLineItemList = [SELECT Id, Apttus_Config2__AttributeValueId__r.Upgrade_To_Aircraft_product__r.Aircraft_Type_Rank__c
                                                                  FROM Apttus_Config2__LineItem__c WHERE ID IN :listLineItemID AND 
                                                           Apttus_Config2__Description__c ='QS Executive' AND (Line_Status_System__c = 'Existing' OR Line_Status_System__c = 'Amended') limit 1];
        } else {
            qsExecLineItemList = [SELECT Id, Apttus_Config2__AttributeValueId__r.Upgrade_To_Aircraft_product__r.Aircraft_Type_Rank__c
                                                                  FROM Apttus_Config2__LineItem__c WHERE ID IN :listLineItemID AND 
                                                           Apttus_Config2__Description__c ='QS Executive' AND Line_Status_System__c = 'New' limit 1];
        }
        if(!qsExecLineItemList.isEmpty()) {
            qsExecLineItem = qsExecLineItemList[0];
        
            system.debug('qsExecLineItem --> '+ qsExecLineItem);                                                   
            Apttus_Config2__LineItem__c atLineItem = new Apttus_Config2__LineItem__c();         
            
            if(modificationType == 'Assignment') {
                atLineItem = [SELECT Id, Apttus_Config2__OptionId__r.Aircraft_Type_Rank__c,Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c
                                                                      FROM Apttus_Config2__LineItem__c WHERE ID IN :listLineItemID AND 
                                                             Apttus_Config2__OptionGroupLabel__c='Aircraft Type' AND (Line_Status_System__c = 'Existing' OR Line_Status_System__c = 'Amended') limit 1]; 
            } else {
                atLineItem = [SELECT Id, Apttus_Config2__OptionId__r.Aircraft_Type_Rank__c,Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c
                                                                      FROM Apttus_Config2__LineItem__c WHERE ID IN :listLineItemID AND 
                                                             Apttus_Config2__OptionGroupLabel__c='Aircraft Type' AND Line_Status_System__c = 'New' limit 1]; 
            }
            
            system.debug('atLineItem --> '+ atLineItem);
            
            Apttus_Proposal__Proposal__c oProposal = new Apttus_Proposal__Proposal__c (Id = atLineItem.Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c);
            oProposal.APTPS_Upgrade_Eligible__c = false;
            
            system.debug('oProposal --> '+ oProposal);

            if(qsExecLineItem!= NULL && atLineItem!=NULL && qsExecLineItem.Apttus_Config2__AttributeValueId__r.Upgrade_To_Aircraft_product__r.Aircraft_Type_Rank__c> atLineItem.Apttus_Config2__OptionId__r.Aircraft_Type_Rank__c){
                        
                system.debug('Inside  -->');
                oProposal.APTPS_Upgrade_Eligible__c = true;
                
            }
            try { 
                system.debug('Inside try  -->' + oProposal);
                update oProposal;
            } catch (DmlException e) {
                System.debug (e);
            }
        }
    }
    //GCM-8289 End
    
     /**
    * Method is used in APTS_ValidationCallback
    * @param cart - used in Validation Callback
    * @return null
    */
   
    @future(callout=true)
    public static void updateOverrideHours (Id cartId) { 
        List <Apttus_Config2__LineItem__c> listLineItems = [Select Apttus_Config2__LineStatus__c,
                Apttus_Config2__ConfigurationId__r.Apttus_QPConfig__Proposald__c,
                Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c,
                Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c,
                Apttus_Config2__AttributeValueId__r.id              
            From Apttus_Config2__LineItem__c
            Where Apttus_Config2__ConfigurationId__c = :cartId
                AND Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c > 0
                AND (Apttus_Config2__ProductId__r.ProductCode = 'NJUS_CARD' OR Apttus_Config2__ProductId__r.ProductCode = 'NJE_CARD')
        ];
        
        List<Apttus_Config2__ProductAttributeValue__c> pavList = new List<Apttus_Config2__ProductAttributeValue__c>();
        set<id> pavIds = new set<id>();
        
        if (!listLineItems.isEmpty ()) {
            for(Apttus_Config2__LineItem__c li: listLineItems) {
                if(!li.Apttus_Config2__AttributeValueId__r.Override_Requested_Hours__c && li.Apttus_Config2__AttributeValueId__r.APTS_Override_Requested_Hours__c > 0) {
                    pavIds.add(li.Apttus_Config2__AttributeValueId__r.id);
                    
                }
            }
            if(!pavIds.isEmpty()) {
                for(id pavId : pavIds) {
                    pavList.add(new Apttus_Config2__ProductAttributeValue__c(id=pavId, APTS_Override_Requested_Hours__c=0));
                }
                
            }
        }
        
        
        try { 
            if(!pavList.isEmpty()){
                update pavList;
            }
            
        } catch (DmlException e) {
            System.debug (e);
        }

        
        
        
        
        

    }

}
/************************************************************************************************************************
@Name: APTPS_CollaborationTriggerHelper
@Author: Conga PS Dev Team
@CreateDate: 20 July 2021
@Description: Collaboration Request trigger helper
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CollaborationTriggerHelper {
    
    /**
    @description: Call email notification framework for Share & Lease collaboration requests.
    @param: collabList - List of Collaboration Request records
    @return: void
    */
    public static void sendCollabEmailNotification(List<Apttus_Config2__CollaborationRequest__c> collabList, boolean isDeleted) {
        for(Apttus_Config2__CollaborationRequest__c cr : collabList) {
            try{
                APTPS_EmailNotification enObj = new APTPS_EmailNotification();
                String currencyCode = cr.CurrencyIsoCode;
                String progType = cr.APTPS_Program_Type__c;
                String prodCode = '';
                String collabStatus = isDeleted ? APTS_ConstantUtil.STATUS_CANCELLED : cr.Apttus_Config2__Status__c;
                //Share product
                if(APTS_ConstantUtil.SHARE.equalsIgnoreCase(progType))
                    prodCode = APTS_ConstantUtil.CUR_USD.equalsIgnoreCase(currencyCode) ? APTS_ConstantUtil.NJA_SHARE_CODE : APTS_ConstantUtil.NJE_SHARE_CODE;
                
                //TODO: Add condition for Lease
                
                enObj.call(prodCode, new Map<String, Object>{'Id' => cr.Id,'Business_Object'=>APTS_ConstantUtil.COLLAB_REQ_OBJECT_API,'Product_Code'=>prodCode,'Status'=>collabStatus});  
            }catch(APTPS_EmailNotification.ExtensionMalformedCallException e) {
                system.debug('Error while sending Email Notification for Collaboration Id --> '+cr.id+' Error details --> '+e.errMsg);
            }
        }
    }

    public static void updateConfig(Set<Id> configIdSet, boolean isCollabCompleted) {
        List<Apttus_Config2__ProductConfiguration__c> configListToUpdate = new List<Apttus_Config2__ProductConfiguration__c>();
        for(Apttus_Config2__ProductConfiguration__c config : [SELECT Id, APTPS_Is_Quote_Collaboration_Completed__c FROM Apttus_Config2__ProductConfiguration__c WHERE Id IN :configIdSet]) {
            config.APTPS_Is_Quote_Collaboration_Completed__c = isCollabCompleted;
            configListToUpdate.add(config);
        }

        system.debug('Updating these configs --> '+configListToUpdate);
        if(!configListToUpdate.isEmpty())
            update configListToUpdate;
    }
}
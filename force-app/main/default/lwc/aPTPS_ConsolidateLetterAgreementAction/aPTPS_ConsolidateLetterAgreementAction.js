import { LightningElement, track, wire, api  } from 'lwc';
import createLAConsolidateRecords from 
  '@salesforce/apex/APTPS_ConsoleLetterAgreementController.createLAConsolidateRecords';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';

export default class APTPS_ConsolidateLetterAgreementAction extends LightningElement {
    @api recordId;
	@track error;
	_title = 'In Progress';
  message = 'Letter Agreement Creation is in progress!! Please check the status after sometime. Email sent to the Owner with the Records attached';
  variant = 'success';

	renderedCallback() { 
		console.log('@21 '+this.recordId);
		this.callCreateLAConsolidateRecords();
	}

	callCreateLAConsolidateRecords() {
		console.log('@21 1'+this.recordId);
		
		if(this.recordId) {
			createLAConsolidateRecords({agreementId: this.recordId})
			.then(() => {
				this.closeQuickAction();
				this.showNotification();
			})
			.catch(error => {
				console.log('@21 error');
				console.log(error);
				this.error = error;
				this._title = 'Error';
				this.message = error;
				this.variant = 'error';
				this.closeQuickAction();
				this.showNotification();
			})
		}
		
	}

	showNotification() {
		const evt = new ShowToastEvent({
			title: this._title,
			message: this.message,
			variant: this.variant,
		});
		this.dispatchEvent(evt);
	}

	closeQuickAction() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}
}